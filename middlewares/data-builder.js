//util/data-builder.js
var self = this;
var api = require('../util/data-api');

self.build = function(data, next) {
    str = '';

    str += api.stringToClientType(data.type) +',';
    delete(data.type);

    for (var k in data) {
        str += api.stringToField(k) +',';
        str += data[k] +',';
    }
    str = str.slice(0, -1);
    str += '\n';
    next (null, str);
};

module.exports = self.build;
