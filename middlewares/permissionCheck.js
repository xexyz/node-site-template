//permissionCheck.js
var moment          = require('moment-timezone'),

    permissionCheck = function(level) {
        var lvl = level !== undefined ? level : 1;
        return function(req, res, next) {
            if (req.session.roleId < lvl){
                res.status(403);
                if (req.accepts('html')) {
                    res.render('403', { url: req.url });
                    return;
                }
                if (req.accepts('json')) {
                    res.send({ error: 'Not authorized' });
                    return;
                }
                return res.type('txt').send('Not authorized');
            }

            if (req.isAuthenticated() && req.session.validTfa){

                if (req.session.profile === undefined){
                    res.status(401);
                    if (req.accepts('html')) {
                        res.render('401', { url: req.url });
                        return;
                    }
                    if (req.accepts('json')) {
                        res.send({ error: 'Not authenticated' });
                        return;
                    }
                    return res.type('txt').send('Not authenticated');
                }

                return next();
            }

            res.redirect('/');
        }
    };

module.exports = permissionCheck;
