//passport.js
var LocalStrategy   = require('passport-local').Strategy,

u_passport,

createPassport = function(passport) {
	if (u_passport)
		return u_passport;

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
		done(null, user.id);
    });
 
    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        models.user
			.find({where: {id: id}})
			.then(function(user){
				done(null, user);
			}), (function(err){
				done(err, null);
			});
    });
	
	passport.use('local-signin', new LocalStrategy({
        passReqToCallback : true },
    function(req, username, password, done) { // callback with email and password from our form

		 if (global.cfg.is_demo){
			if (username.substring(username.length - 5) != '-demo'){
				logger.info('Login attempt failed', {'reason' : 'wrong demo account syntax', 'userName' : username});
				return done(null, false);
			}
			else
				username = username.slice(0, -5);
		 }
        models.user
			.find({where: {username: username}, include: [models.company]})
			.then(function(user){
				if (!user) {
					logger.info('Login attempt failed', {'reason' : 'user not found', 'userName' : username});
					return done(null, false);
				}
				if (password != user.password){
					logger.info('Login attempt failed', {'reason' : 'wrong password', 'userName' : username});
					return done(null, false);
				}

				// if (!global.cfg.is_demo && rows[0].demo_only){
				// 	logger.log('info', 'Login attempt failed', {'reason' : 'user is demo only', 'userName' : username});
				// 	return done(null, false, {message: 'Authentication failure: user not permissioned for live access'});
				// }

				//todo update lastlogin for user
				done(null, user);
			}, function(err){
				done(err, null);
			});
    }));
 
// IF SIGNUP IS NEEDED
    /*passport.use('local-signup', new LocalStrategy(
		{passReqToCallback : true  },
    function(req, username, password, done) {

        db.client.query('SELECT * FROM users WHERE username = ''+username+''',function(err,rows){
			if (err)
                return done(err);
			 if (rows.length) {
                return done(null, false, {message: 'That username is already taken.'});
            } else {
 
				// if there is no user with that email
                // create the user
                var newUserMysql = new Object();
				
				newUserMysql.username    = username;
                newUserMysql.password = password; // use the generateHash function in our user model
			
				var insertQuery = 'INSERT INTO users ( username, password ) values ('' + username +'',''+ password +'')';
				connection.query(insertQuery,function(err,rows){
				newUserMysql.id = rows.insertId;
				
				return done(null, newUserMysql);
				});	
            }	
		});

    }));*/
    u_passport = passport;
    return u_passport;
 };

module.exports = createPassport;
