//timeCheck.js
var moment          = require('moment-timezone');

this.timeCheck = function(req, res, next) {

        var tz = moment().tz("America/New_York").format();
        var date = moment(tz);
        var hr = date.hour();
        var day = date.day();

        if ((global.cfg.op_hours.hours.indexOf(hr) > -1
            || global.cfg.op_hours.days.indexOf(day) > -1)
            && global.cfg.op_hours.restrict_hours){
            logger.info('Homepage requested after hours, redirecting');
            return res.render('index/index-off');
        }
        else
            return next();
    };

module.exports = this.timeCheck;