//browserCheck.js
var    UAParser 		= require('ua-parser-js'),

    browserCheck = function(req, res, next) {
        var parser = new UAParser();
        var ua = req.headers['user-agent'];
        var result = parser.setUA(ua).getResult();

        logger.log('info', 'Browser Check', { "browserName" : result.browser.name, "browserVersion" : result.browser.version });

        //only these browsers are allowed.
        if ((result.browser.name == 'Chrome' && result.browser.major > 35)
            || (result.browser.name == 'Firefox' && result.browser.major > 34)
            || (result.browser.name == 'IE' && result.browser.major > 9)
            || (result.browser.name == 'Mobile Safari' && result.browser.major > 6)
            || (result.browser.name == 'Safari' && result.browser.major > 7))
            return next();

        logger.log('info', 'Browser type not allowed, redirecting', { "browserName" : result.browser.name, "browserVersion" : result.browser.version });
        res.render('index/index-bad-browse');
    };

module.exports = browserCheck;
