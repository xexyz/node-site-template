//util/data-parser.js
var self = this;
var api = require('../util/data-api');

self.parser = function(str, next) {
    data = {};
    var parts = str.split(',');

    data.type = api.serverTypeToString(parts[0]);
    for (var j = 1; j < parts.length; j += 2) {
        data[api.fieldToString(parts[j])] = parts[j + 1];
    }
    next (null, data);
};

module.exports = self.parser;
