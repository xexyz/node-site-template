//middleware/data-pipeline.js
var async 		    = require('async');
var Decimal         = require('decimal.js');

var self = this;

var client          = require('redis').createClient(global.cfg.redis_store_options);
var apiSub          = require('redis').createClient(global.cfg.redis_store_options);
var reqSub          = require('redis').createClient(global.cfg.redis_store_options);

var exId = -1;
var subTy = 0;
var impSt = 8;
var fvSt = 2;

self.subscribe = function(message){
    var data = JSON.parse(message);

    async.waterfall([
        function(done) { //get the addon
            models.sequelize.query('CALL spGetInstrAddOns(' + data.companyId + ',' + data.instrumentId + ')')
                .then(function(users) {
                    if (users.length > 0) {
                        done(null, JSON.parse(JSON.stringify(users)));
                    }else{
                        done('not found');
                    }
                }, function(err){
                    logger.error(err);
                })
            ;
        },
        function(addon, done){ //get the brokerage
            //todo the brokerage
            done(null, addon, 300);
        },
        function(addon, brokerage, done){ //setup request
            models.instrument
                .find({where: {id: data.instrumentId}, include: [models.product]})
                .then(function(instrument){
                    var apiName;
                    var n1 = instrument.monthYearCode1.charAt(0) + instrument.monthYearCode2.charAt(2);
                    var n2 = instrument.monthYearCode2.charAt(0) + instrument.monthYearCode2.charAt(2);
                    var p = instrument.product.apiName.split('-');
                    switch(instrument.instrumentTypeCode){
                        case 'F':
                            apiName = p[0] + n1;
                            break;
                        case 'S':
                            apiName = p[0] + n1 + '-' + p[0] + n2;
                            break;
                        default:
                            apiName = p[0] + n1 + '-' + p[1] + n2;
                            break;
                    }
                    var mess = {
                        type: 'get_price_bbo',
                        instrument_name: apiName,
                        exchange_id: exId,
                        subscription_type: 0,
                        context_id: data.contextId
                    }
                    client.publish('send_to_api', JSON.stringify(mess));

                    mess = {
                        type: 'get_imp_price',
                        instrument_name: apiName,
                        exchange_id: exId,
                        subscription_type: 1,
                        context_id: data.contextId,
                        ip_size_to_avg : data.quantity,
                        imp_price_style: impSt,
                        fv_style: fvSt,
                        market_depth: 1
                    };
                    client.hmset('pl:' + data.contextId, {
                        name: apiName,
                        quantity: data.quantity,
                        brokerage: brokerage,
                        addon: addon[0].addedTicks,
                        fee: Decimal(data.quantity) * Decimal(instrument.product.blockFee)
                    });
                    client.publish('send_to_api', JSON.stringify(mess));
                }, function (err){

                })
            ;
        }
    ], function(err, result) {
        // if (err)
        //     logger.error(err);
    });

};

self.unsubscribe = function(message){
    var data = JSON.parse(message);
};

self.pricePipeline = function(message){
    var data = JSON.parse(message);

    async.waterfall([
        function(done) {
            client.hgetall('pl:' + data.context_id, function (err, data) {
                if(err)
                    done(err)
                else
                    done(data);
            });
        },
        function(pl, done){
            var parts = data.context_id.split('_');
            //todo some adjustments
            var mess = {
                contextId: data.context_id,
                rowId: parts[2],
                bid: data.bid,
                ask: data.ask
            }

            client.publish('imp_price_adj', JSON.stringify(mess));
        }
    ], function(err, result) {

    });
};

// apiSub.subscribe('login_response');
// apiSub.subscribe('bbo');
apiSub.subscribe('imp_price');
apiSub.on("message", function (channel, message) {
    switch(channel){
        case 'imp_price':
            self.pricePipeline(message);
            break;
        default:
            break;
    }

});

reqSub.subscribe('price_subscribe');
reqSub.subscribe('price_unsubscribe');
reqSub.on("message", function (channel, message) {
    switch(channel){
        case 'price_subscribe':
            self.subscribe(message);
            break;
        case 'price_unsubscribe':
            self.unsubscribe(message);
            break;
        default:
            break;
    }
});

self.setup = function() {
    logger.info('Pricing pipeline initialized.')
};

module.exports = self.setup;
