# use the tools as dev dependencies rather than installing them globaly
# it lets you handle specific versions of the tooling for each of your projects
MOCHA=node_modules/mocha/bin/mocha
ISTANBUL=node_modules/.bin/istanbul
_MOCHA=node_modules/mocha/bin/_mocha
ISTANBUL_WIN=node_modules/.bin/istanbul

PWDPATH =  $(shell pwd)
SONARPATH = sonar-scanner-2.8/bin

# test files must end with ".test.js"
TESTS=$(shell find test/ -name "*.tests.js")
#TESTS=$(shell echo test/middlewares/*.tests.js)
SOURCES = $(shell echo src/*.cpp) 
REPORTS = $(reports/)
clean:
	rm -rf reports

clean_sonar:
	rm -rf sonar-scanner-2.8
	rm sonar-scanner-2.8*
	
test:
	$(MOCHA) -R spec $(TESTS)



coverage:
	@# check if reports folder exists, if not create it
	@test -d reports || mkdir reports
	$(ISTANBUL) cover --report lcovonly --dir ./reports $(MOCHA) -- -R spec $(TESTS)
	
coverage_win:
	@# check if reports folder exists, if not create it
	@test -d reports || mkdir reports
	$(ISTANBUL_WIN) cover --report lcovonly --dir ./reports $(_MOCHA) -- -R spec $(TESTS)


	
sonar:
	$(shell cd $(SONARPATH); sh sonar-scanner)

       
       


	