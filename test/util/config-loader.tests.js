require('../helpers/setup.js')
var expect = require('chai').expect;
var sinon = require('sinon');
var proxyquire = require('proxyquire');
var configloader;

describe("util/config-loader.js", function() {
    var cu, cp, bu, bp;
    var request;

    before('base setup', function() {
        request = sinon.stub();
        configloader = proxyquire('../../util/config-loader.js',{'request': request});
        cu = 'URL HERE';
        cp = 'dev';
        bu = 'URL HERE';
        bp = 'generic';
    });


    //Correct case test
    it('should return a vaild config', function(done) {
        var expectedparameter = {
          url: "URL HERE";
         json: true
        };
        var body = JSON.stringify({
            company_name: "Generic",
            company_short: "generic",
            file_prefix: "generic",
            gui_vars: {
                app_name: "Data Direct",
                company_name: "COMPANY"
            },
            sidecar: {
                port: 80
            },
            spring: {
                application: {
                    name: "data-direct"
                }
            }
        });
        request.withArgs(expectedparameter).yields(null, {statusCode : 200}, body);
        configloader(bu, bp, function(err, body){
            expect(body).to.equal(JSON.stringify({
                "company_name": "Generic",
                "company_short": "generic",
                "file_prefix": "generic",
                "gui_vars": {
                    "app_name": "Data Direct",
                    "company_name": "COMPANY"
                },
                "sidecar": {
                    "port": 80
                },
                "spring": {
                    "application": {
                        "name": "data-direct"
                    }
                }
            }));
        });
        done();
    });

    //Not found the end point
    it('json data not match test', function(done) {
        var expectedparameter = {
            url: "URL HERE",
            json: true
        };
        var expectedError = new Error('Not found');
        request.withArgs(expectedparameter).yields(expectedError, null, null);
        configloader(cu, bp, function(err, body){
            expect(err).to.equal(expectedError);
            expect(body).to.be.null;
        });
        done();
    });

    //Endpoint Lost profile case
    it('json data not match test', function(done) {
        var expectedparameter = {
            url: "URL HERE",
            json: true
        };
        var expectedError = new Error('Not found');
        request.withArgs(expectedparameter).yields(expectedError, null, null);
        configloader(cu, '', function(err, body){
            expect(err).to.equal(expectedError);
            expect(body).to.be.null;
        });
        done();
    });

    //Empty case test
    it('Empty Case test', function(done) {
        var expectedparameter = {
            url: "",
            json: true
        };
        var expectedError = new Error('Not Valid Endpoint');
        request.withArgs(expectedparameter).yields(expectedError, null, null);
        configloader('', '', function(err, body){
            expect(err).to.equal(expectedError);
            expect(body).to.be.null;
        });
        done();
    });
});
