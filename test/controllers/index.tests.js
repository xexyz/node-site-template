require('../helpers/setup.js')

var controller;

describe("controllers/index.js", function() {
    var req, res;

    before('base setup', function() {
        global.cfg.api_is_live = false;
        controller = require('../../controllers/index.js');
    });

    beforeEach(function(){
        req = res = {};

        req.flash = sinon.spy(req.flash);
        req.isAuthenticated = sinon.spy(req.isAuthenticated);

        res.type = sinon.spy(res.type);
        res.render = sinon.spy(res.render);
        res.send = sinon.spy(res.send);
        res.redirect = sinon.spy(res.redirect);
    });

    describe("GET no_robots", function() {
        it('should return instance', function() {
            controller.no_robots(req, res);

            res.send.calledOnce.should.equal(true);
            res.send.lastCall.args[0].should.equal("User-agent: *\nDisallow: /");
        });;
    });

    describe("GET index", function() {
        it('should return instance', function() {
            controller.index(req, res);
            res.redirect.calledOnce.should.equal(true);
        });;
    });

    describe("GET logout", function() {
        it('should return instance', function() {
            controller.index(req, res);
            res.redirect.calledOnce.should.equal(true);
        });;
    });
});
