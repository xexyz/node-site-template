require('../helpers/setup.js')
var expect = require('chai').expect;
var should = require('chai').should();
var controller;
describe("controllers/login.js", function() {
    var req, res;

    before('base setup', function() {
        controller = require('../../controllers/login.js');
        request = sinon.stub();
    });

    beforeEach(function(){
        res = sinon.useFakeXMLHttpRequest();
        req = sinon.useFakeXMLHttpRequest();
        req.url = "test/url";
        req.isAuth = false;

        req.isAuthenticated = function(){return req.isAuth};

        req.accepts = function(type){
            if ( req.requestHeaders.Accept.indexOf(type)>=0)
                return true;
            return false;
        }

        req.type = function(type){
            return req;
        }

        res.status = function(status){res.statusCodes = status}

        req.session = {'roleId': -1, 'validTfa': false, 'profile': undefined};
        req.flash = sinon.spy(req.flash);
        res.send = sinon.spy(res.send);
        res.render = sinon.spy(res.render);
        res.redirect = sinon.spy(res.redirect);
        nextSpy = sinon.spy();
    });

    //Login failed1(Everything false)
    describe("GET login1", function() {
        it('should return login/login', function() {
            controller.index(req, res);
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('login/login');
        });;
    });
    //Login failed2(validTfa false)
    describe("GET login2", function() {
        it('should return login/login', function() {
            req.isAuth = true;
            req.isAuthenticated = function(){return req.isAuth};
            controller.index(req, res);
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('login/login');
        });;
    });
    //Login failed3(Request is Empty)
    describe("GET login3", function() {
        it('should return login/login', function() {
            req = {};
            req.isAuthenticated = function(){return req.isAuth};
            controller.index(req, res);
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('login/login');
        });;
    });

    //Login Suceess
    describe("Login Success", function() {
        it('should return /home', function() {
            req.isAuth = true;
            req.isAuthenticated = function(){return req.isAuth};
            req.session.validTfa = true;
            controller.index(req, res);
            res.render.calledOnce.should.equal(false);
            res.redirect.calledOnce.should.equal(true);
            res.redirect.lastCall.args[0].should.equal('/home');
        });;
    });

    //Redirect to Login
    describe("Redirect to Login", function() {
        it('should return /login', function() {
            controller.tfa(req, res);
            res.render.calledOnce.should.equal(false);
            res.redirect.calledOnce.should.equal(true);
            res.redirect.lastCall.args[0].should.equal('/login');
        });;
    });

    //Redirect to home
    describe("Redirect to Home", function() {
        it('should return /home', function() {
            req.isAuth = true;
            req.isAuthenticated = function(){return req.isAuth};
            req.session.validTfa = true;
            controller.tfa(req, res);
            res.render.calledOnce.should.equal(false);
            res.redirect.calledOnce.should.equal(true);
            res.redirect.lastCall.args[0].should.equal('/home');
        });;
    });

    //Get TFA
    describe("GET TFA", function() {
        it('should return login/tfa', function() {
            req.isAuth = true;
            req.isAuthenticated = function(){return req.isAuth};
            req.session.validTfa = false;
            controller.tfa(req, res);
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('login/tfa');
        });;
    });

    //auth1 Setup
    describe("Set Authorization1", function(){
        it("Setup session profile", function(){
            req.user = {};
            req.session = {};
            req.user.username = "Test";
            req.user.email = "test@test.com";
            req.user.roleId = 1;
            req.user.userId = 1;
            req.user.company = {};
            req.user.company.companyName = "COMPANY"
            req.user.firstName = "Test";
            req.user.lastName = "Test";
            req.user.isInternal = 1;
            req.user.viewOnly = 0;
            req.user.id = 1;
            req.user.companyId = 1;
            req.user.authyId = 1;
            req.user.tfaValidUntil = moment('2016-01-01T23:35:01');
            controller.auth1(req, res);
            should.exist(req.session.profile);
            expect(req.session.profile.username).to.equal(req.user.username);
            expect(req.session.profile.email).to.equal(req.user.email);
            expect(req.session.profile.roleId).to.equal(req.user.roleId);
            expect(req.session.profile.userId).to.equal(req.user.userId);
            expect(req.session.profile.companyName).to.equal(req.user.company.companyName);
            expect(req.session.profile.fullName).to.equal(req.user.firstName + ' ' + req.user.lastName);
            expect(req.session.isInternal).to.equal(true);
            expect(req.session.viewOnly).to.equal(false);
            expect(req.session.userId).to.equal(req.user.id);
            expect(req.session.companyId).to.equal(req.user.companyId);
            expect(req.session.roleId).to.equal(req.user.roleId);
            expect(req.session.authyId).to.equal(req.user.authyId);
            expect(req.session.validTfa).to.equal(moment.utc().isBefore(moment(req.user.tfaValidUntil)));
            res.send.calledOnce.should.equal(true);
        })
    });
    //
    // describe("Authy Verify", function(){
    //     it("should return error code", function(){
    //         var authy = require('authy')(global.cfg.authy_token,
    //             (!global.cfg.is_dev && 'http://sandbox-api.authy.com'));
    //         req.session.authyId = 123;
    //         req.body = {};
    //         req.body.token = '';
    //         req.body.remember = false;
    //         var id = req.session.authyId;
    //         var token = req.body.token;
    //         var expectedError = new Error('Token Cannot Be Null or Empty');
    //         request.withArgs().yields(expectedError, null);
    //         authy.verify(id, token, function(err, body){
    //             res.status.should.equal(res.sendStatus(401));
    //         });
    //         done();
    //     });
    // });

});
