require('../helpers/setup.js')

var controller;

describe("controllers/forgot.js", function() {
    var req, res;

    before('base setup', function() {
        controller = require('../../controllers/forgot.js');
    });

    beforeEach(function(){
        req = res = {};

        req.flash = sinon.spy(req.flash);
        req.isAuthenticated = sinon.spy(req.isAuthenticated);

        res.type = sinon.spy(res.type);
        res.render = sinon.spy(res.render);
        res.send = sinon.spy(res.send);
        res.redirect = sinon.spy(res.redirect);
    });

    describe("GET forgot", function() {
        it('should return forgot/forgot', function() {
            controller.index(req, res);
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('forgot/forgot');
        });;
    });

});