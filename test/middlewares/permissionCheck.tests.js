/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require('../helpers/setup.js')
var moment = require('moment-timezone')
var middleware;


describe("middleware/permissionCheck.js", function(){
    var req, res, nextSpy, level, nextMiddleware;
    
    before("base setup", function(){
        middleware = require('../../middlewares/permissionCheck.js');
    });
    
    beforeEach(function(){

        res = sinon.useFakeXMLHttpRequest();
        req = sinon.useFakeXMLHttpRequest();
        req.url = "test/url";
        req.isAuth = false;
        
        req.isAuthenticated = function(){return req.isAuth};
        
        req.accepts = function(type){
            if ( req.requestHeaders.Accept.indexOf(type)>=0)
                return true;
            return false;
        }
       
       req.type = function(type){
           return req;
       }
       
     
       res.status = function(status){res.statusCodes = status}
       
       req.session = {'roleId': -1, 'validTfa': false, 'profile': undefined};
       res.send = sinon.spy(res.send);
       res.render = sinon.spy(res.render);
       res.redirect = sinon.spy(res.redirect);
       nextSpy = sinon.spy();
    });
    
    describe("roleId less than level", function(){
        level = 3;
         
        describe("req accept html", function(){
            it("should return 403 and not authorized", function(){     
            nextMiddleware = middleware(level);
            req.session.roleId = 2;
            req.requestHeaders = {
                "Accept": ["html"],
             };
            nextMiddleware(req, res, nextSpy);
     
           
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('403');
            res.render.lastCall.args[1].url.should.equal(req.url);
            });
        });
        
         describe("req accept json", function(){
            it("should call res.send", function(){    
            nextMiddleware = middleware(level);
            req.session = {'roleId': 2};
             req.requestHeaders = {
                "Accept": ["json"],
             };
            nextMiddleware(req, res, nextSpy);
            res.send.calledOnce.should.equal(true);
            });
        });
        
         describe("req accept txt", function(){
            it("should call res type(txt) send", function(){    
            nextMiddleware = middleware(level);
            req.session = {'roleId': 2};
             req.requestHeaders = {
                "Accept": ["txt"],
             };
            nextMiddleware(req, res, nextSpy);
            res.send.calledOnce.should.equal(true);
            });
        });
               
        
    });
    
    describe("isAuthenticated and req.session.validTfa test", function(){
        level = 3;
            
        describe("profile undifiend",function(){
            
            
            describe("req accecpt html", function(){
                 it ("should return 401 and not authorized", function(){
                   
                    req.session.roleId = 4;
                    req.session.validTfa = true;
                    req.isAuth = true;
                    nextMiddleware = middleware(level);

                    req.requestHeaders = {
                    "Accept": ["html"],
                    };
                    nextMiddleware(req, res, nextSpy);
                    res.render.calledOnce.should.equal(true);
                    res.render.lastCall.args[0].should.equal('401');
                    res.render.lastCall.args[1].url.should.equal(req.url);
                 });
   
            });

            describe("req accept json", function(){
                it("should call res send", function(){
                    req.session.roleId = 4;
                    req.session.validTfa = true;
                    req.isAuth = true;
                    nextMiddleware = middleware(level);

                    req.requestHeaders = {
                    "Accept": ["json"],
                    };
                    nextMiddleware(req, res, nextSpy);
                    res.send.calledOnce.should.equal(true);
                });
            });

            describe("req accept txt", function(){
                it("should call res send", function(){
                    req.session.roleId = 4;
                    req.session.validTfa = true;
                    req.isAuth = true;
                    nextMiddleware = middleware(level);

                    req.requestHeaders = {
                    "Accept": ["txt"],
                    };
                    nextMiddleware(req, res, nextSpy);
                    res.send.calledOnce.should.equal(true);
                });
            });
        });
        
        describe("valid permission check", function(){
            it("should call next", function(){
                req.session.profile = "test";
                req.session.roleId = 4;
                req.session.validTfa = true;
                req.isAuth = true;
                nextMiddleware = middleware(level);
                nextMiddleware(req, res, nextSpy);
                nextSpy.calledOnce.should.equal(true);
            });
        });
    });
        

    describe("Invalid permission check", function(){
            level = 3;
            it("should call redirect", function(){
              
                req.session.roleId = 4;
                req.session.validTfa = false;
                req.isAuth = false;
                nextMiddleware = middleware(level);
                nextMiddleware(req, res, nextSpy);
                res.redirect.calledOnce.should.equal(true);
            })
    });
});



