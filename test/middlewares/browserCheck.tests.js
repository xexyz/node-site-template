require('../helpers/setup.js')

var middleware;

describe("middleware/browserCheck.js", function() {
    var req, res, nextSpy;

    before('base setup', function() {
        middleware = require('../../middlewares/browserCheck.js');
    });

    beforeEach(function(){
        req = res = {};
        res.render = sinon.spy(res.render);
        req.headers = {}
        nextSpy = sinon.spy();
    });

    describe("Restricted browsers", function() {
        it('should return index-off', function() {
            req.headers['user-agent'] = 'asdf';
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-bad-browse');
            nextSpy.calledOnce.should.equal(false);
        });
    });

//     describe("User agent not set", function() {
//         it('should return index-off', function() {
//             middleware(req, res, nextSpy);
//    
//             res.render.calledOnce.should.equal(true);
//             res.render.lastCall.args[0].should.equal('index/index-bad-browse');
//             nextSpy.calledOnce.should.equal(false);
//         });
//     });

    describe("Allowed chrome browsers", function() {
        it('should return next()', function() {
            req.headers['user-agent'] = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0';
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(false);
            nextSpy.calledOnce.should.equal(true);
        });;
        
        it('should return index-off by wrong version', function(){
            req.headers['user-agent'] = 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35';
             middleware(req, res, nextSpy);
             
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-bad-browse');
            nextSpy.calledOnce.should.equal(false);
        });
    });
    
    describe("Allowed Safari browsers", function() {
        it('should return next()', function() {
            req.headers['user-agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/8.0.3 Safari/600.3.18';
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(false);
            nextSpy.calledOnce.should.equal(true);
         });
         
         it('should return index-off by wrong version', function(){
           req.headers['user-agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/600.3.18 (KHTML, like Gecko) Version/7.0.3 Safari/600.3.18';
             middleware(req, res, nextSpy);
             
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-bad-browse');
            nextSpy.calledOnce.should.equal(false);
        });
    });
    
    describe("Allowed Firefox browsers", function() {
        it('should return next()', function() {
            req.headers['user-agent'] = 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0';
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(false);
            nextSpy.calledOnce.should.equal(true);
        });
        
        it('should return index-off by wrong version', function(){
            req.headers['user-agent'] = 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/34.0';
            middleware(req, res, nextSpy);
             
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-bad-browse');
            nextSpy.calledOnce.should.equal(false);
        });
        
        
    });
    
    describe("Allowed IE browsers", function() {
        it('should return next()', function() {
            req.headers['user-agent'] = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(false);
            nextSpy.calledOnce.should.equal(true);
        });
        
        it('should return index-off by wrong version', function(){
            req.headers['user-agent'] = 'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.2; Trident/6.0)';
            middleware(req, res, nextSpy);
             
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-bad-browse');
            nextSpy.calledOnce.should.equal(false);
        });
    });
    
    describe("Allowed Mobile Safari browsers", function() {
        it('should return next()', function() {
            req.headers['user-agent'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/7.0 Mobile/10B350 Safari/8536.25';
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(false);
            nextSpy.calledOnce.should.equal(true);
        });
        
        it('should return index-off by wrong version', function(){
            req.headers['user-agent'] = 'Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350 Safari/8536.25';
            middleware(req, res, nextSpy);
             
            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-bad-browse');
            nextSpy.calledOnce.should.equal(false);
        });
    });
    
    
});