/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require('../helpers/setup.js')
var expect = require("chai").expect;
var middleware, api;

describe("middleware/data-parser.js", function(){
    var nextSpy, data, dataParse;
    
    before("base setup", function(){
        middleware = require('../../middlewares/data-parser.js');
        api = require("../../util/data-api.js");
        nextSpy = sinon.spy();
        
        data = "1010,1,andrew,2,test";
        
        dataParse = {"type":"login_response",
                "user":"andrew",
                "password":"test"
               };
        
    });
    
    
    describe("middleware parse test", function(){
       it("shoud equal", function(){
           middleware(data,nextSpy);
           nextSpy.calledOnce.should.equal(true);
           expect(nextSpy.lastCall.args[0]).to.equal(null);
           expect(nextSpy.lastCall.args[1]).to.deep.equal(dataParse);
       }); 
    });
});
