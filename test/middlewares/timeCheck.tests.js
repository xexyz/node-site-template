require('../helpers/setup.js')

var middleware;

describe("middleware/timeCheck.js", function() {
    var req, res, nextSpy;

    before('base setup', function() {
        middleware = require('../../middlewares/timeCheck.js');
    });

    beforeEach(function(){
        req = res = {};
        res.render = sinon.spy(res.render);
        nextSpy = sinon.spy();

        global.cfg.op_hours = {
            'restrict_hours' : true,
            'days' : [0, 1, 2, 3, 4, 5, 6],
            'hours' : [] //eastern time
        };
    });

    describe("Restrict by day", function() {
        it('should return index-off', function() {
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-off');
            nextSpy.calledOnce.should.equal(false);
        });
    });

    describe("Allow by day", function() {
        it('should return next()', function() {
            global.cfg.op_hours.restrict_hours = false;
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(false);
            nextSpy.calledOnce.should.equal(true);
        });;
    });

    describe("Restrict by hour", function() {
        it('should return index-off', function() {
            global.cfg.op_hours.days = [];
            global.cfg.op_hours.hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(true);
            res.render.lastCall.args[0].should.equal('index/index-off');
            nextSpy.calledOnce.should.equal(false);
        });
    });

    describe("Allow by hour", function() {
        it('should return next()', function() {
            global.cfg.op_hours.restrict_hours = false;
            global.cfg.op_hours.days = [];
            global.cfg.op_hours.hours = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23];
            middleware(req, res, nextSpy);

            res.render.calledOnce.should.equal(false);
            nextSpy.calledOnce.should.equal(true);
        });;
    });
});