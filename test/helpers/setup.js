global.sinon = require('sinon');
global.chai = require('chai');
global.should = require('should');

before(function(){
    global.cfg = {};
    global.cfg.is_test = true;
    global.logger = require('../../util/logger.js')()
});