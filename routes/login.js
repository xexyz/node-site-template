var controller      = require('../controllers/login.js');
var passport        = require('../middlewares/passport.js')();
var express 		= require('express');
var router 			= express.Router();

router.get('/', timeCheck, controller.index);
router.post('/', timeCheck, passport.authenticate('local-signin'), controller.auth1);
router.get('/tfa', timeCheck, controller.tfa);
router.post('/tfa', controller.auth2);

module.exports = router;