var controller      = require('../controllers/settings.js');
var timeCheck       = require('../middlewares/timeCheck.js');
var express 		= require('express');
var router 			= express.Router();

router.get('/', timeCheck, permissionCheck(1), controller.index);
router.get('/profile', timeCheck, permissionCheck(1), controller.profile);
router.get('/accounts', timeCheck, permissionCheck(1), controller.accounts);
router.get('/permissions', timeCheck, permissionCheck(1), controller.permissions);
router.get('/notifications', timeCheck, permissionCheck(1), controller.notifications);
router.get('/security', timeCheck, permissionCheck(1), controller.security);

module.exports = router;