var controller      = require('../controllers/forgot.js')
var express 		= require('express')
var router 			= express.Router()

router.get('/', controller.index);
router.post('/', controller.request);
router.get('/:token', controller.reset);
router.post('/:token', controller.set);

module.exports = router;