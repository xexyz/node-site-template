var controller      = require('../controllers/support.js');
var timeCheck       = require('../middlewares/timeCheck.js');
var express 		= require('express');
var router 			= express.Router();

router.get('/', timeCheck, controller.index);

module.exports = router;