var controller      = require('../controllers/signup.js')
var express 		= require('express')
var router 			= express.Router()

router.get('/', controller.index)
router.get('/:token', controller.token);
//router.post('/invite', controller.invite);

module.exports = router;