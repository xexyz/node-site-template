var controller      = require('../controllers/index.js');
var express 		= require('express');
var router 			= express.Router();

router.get('/', controller.index);
router.get('/logout', controller.logout);
router.get('/robots.txt', controller.no_robots);

module.exports = router;