var controller      = require('../controllers/admin.js');
var express 		= require('express');
var router 			= express.Router();

router.get('/', timeCheck, permissionCheck(2), controller.index);
router.get('/status', timeCheck, permissionCheck(2), controller.status);
router.get('/activity', timeCheck, permissionCheck(2), controller.activity);
router.get('/invite', timeCheck, permissionCheck(2), controller.invite);

router.post('/invite', permissionCheck(2), controller.sendInvite)

module.exports = router;