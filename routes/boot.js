// routes/boot.js
var express                 = require('express');
var favicon                 = require('serve-favicon');
var expressWinston 	        = require('express-winston');
var path                    = require('path');
var fs                      = require('fs');

module.exports = function(app) {
// session and stuff =============================================================
    app.use(function(req, res, next) {
        res.locals.profile = req.session.profile;
        res.locals.path = req.path;
        next();
    });

// winston ======================================================================
    expressWinston.requestWhitelist.push('session');
    expressWinston.requestWhitelist.push('body');
    expressWinston.bodyBlacklist.push('password');

    app.use(expressWinston.logger({
        winstonInstance: logger,
    }));


// routes ======================================================================
    //static
    var favi = path.join(__dirname, '../public/favicon/' + global.brd.file_prefix + '.ico')
    if (fs.existsSync(favi))
        app.use(favicon(favi));
    else
        app.use(favicon(path.join(__dirname, '../public/favicon/basic.ico')));
    app.use('/ui',           express.static(path.join(__dirname, '../semantic/dist')));
    app.use('/js',           express.static(path.join(__dirname, '../public/js')));
    app.use('/css',          express.static(path.join(__dirname, '../public/css')));
    app.use('/audio',        express.static(path.join(__dirname, '../public/audio')));

    //http
    app.use('/',             require('./index.js'));
    app.use('/signup',       require('./signup.js'));
    app.use('/login',        require('./login.js'));
    app.use('/forgot',       require('./forgot.js'));
    app.use('/home',         require('./home.js'));
    app.use('/support',      require('./support.js'));
    app.use('/settings',     require('./settings.js'));
    app.use('/admin',        require('./admin.js'));

    //websocket
    require('../controllers/home-io.js')(app);

    // Four hundred Four
    app.use(function(req, res, next){
        res.status(404);
        if (req.accepts('html')) {
            res.render('404', { url: req.url });
            return;
        }
        if (req.accepts('json')) {
            res.send({ error: 'Not found' });
            return;
        }
        res.type('txt').send('Not found');
    });

    //Five Hundred
    app.use(expressWinston.errorLogger({
        winstonInstance: logger,
    }));

    app.use(function(err, req, res, next) {
        logger.error(err);
        res.statusCode = 500;
        res.render('500', {
            url: req.url,
            message: err.message,
            error: (global.cfg.is_dev) ? err : {}
        });
    });

}
