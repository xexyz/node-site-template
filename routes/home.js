var controller      = require('../controllers/home.js');
var express 		= require('express');
var router 			= express.Router();

router.get('/', timeCheck, permissionCheck(1), controller.index);
router.get('/requestsub/:instId', permissionCheck(1), controller.requestsub);
router.get('/tabs', permissionCheck(1), controller.tabs);
router.get('/tabs/:tabId', permissionCheck(1), controller.rows);
router.post('/tabs', permissionCheck(1), controller.newtab);
router.post('/tabs/:tabId', permissionCheck(1), controller.savetab);

module.exports = router;
