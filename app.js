// app.js
// initialize  ======================================================================
// USE FOR REMOTE CONFIG FILE LOADING
var boot = require('./config/config.boot.js')
var cfgLoader = require('./util/config-loader.js');

var loadConfig = new Promise(function(resolve, reject){
    cfgLoader(boot.config_url, boot.config_profile, function(err, data){
        if (err || !data)
            reject(err);
        else{
            resolve(data);
        }
    });
});

var loadBrand = new Promise(function(resolve, reject){
    cfgLoader(boot.brand_url, boot.brand_profile, function(err, data){
        if (err || !data)
            reject(err);
        else{
            resolve(data);
        }
    });
});

Promise.all([loadConfig,loadBrand]).then(function(results)
{
    global.cfg = results[0];
    global.brd = results[1];


// set up ======================================================================
    var express             = require('express.oi');

    var passport            = require('passport');
    var cookieParser        = require('cookie-parser');
    var bodyParser          = require('body-parser');
    var session             = require('express-session');
    var redisStore          = require('connect-redis')(session);
    var helmet              = require('helmet')

    var fs                  = require('fs');

// global tools ================================================================
    global.logger = require('./util/logger.js')();
    global.alerts = require('./util/alerts.js');
    global.timeCheck = require('./middlewares/timeCheck.js');
    global.permissionCheck = require('./middlewares/permissionCheck.js');
    global.models = require('./models');
    global.moment = require('moment');

    var dir = './logs';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

// configuration ===============================================================
    var app = express().http().io();
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(helmet());

// view engine ==================================================================
    app.set('view engine', 'pug');
    app.set('views', './views')

// session store ==================================================================
    if (global.cfg.is_dev)
        redis.flushall();

    app.io.session({
        store: new redisStore(global.cfg.redis_store_options),
        secret: 'allyourbasearebelongtous',
        resave: true,
        saveUninitialized: true,
        name: 'sessionId'
    });

// branding variables =============================================================
    app.locals.vars = global.brd.gui_vars;
    app.locals.moment = require('moment');

// passport ====================================================================
    app.use(passport.initialize()); // startup passport
    app.use(passport.session()); // persistent login sessions
    require('./middlewares/passport')(passport);

// API =====================================================================
// REALTIME API CONNECTIONS AND PIPELINE
//    require('./util/template-conns')();
//    require('./middlewares/template-pipeline');

// routes ======================================================================
    require('./routes/boot.js')(app);

// launch ======================================================================
    app.listen(global.cfg.http_port, function () {
        alerts.send('startup');
        logger.info('The magic happens on port ' + global.cfg.http_port);
    });
}).catch (function(error){
    console.log('Problem loading configuration:' , error)
});

