'use strict';

module.exports = function(sequelize, DataTypes) {
    var UserHistory = sequelize.define('userHistory', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        message:{
            type: DataTypes.TEXT
        },
        timestamp: {
            type: DataTypes.DATE
        }
    }, {
        classMethods: {
            associate: function(models) {
                UserHistory.belongsTo(models.user);
                UserHistory.belongsTo(models.userHistoryType);
            }
        }
    });

    return UserHistory
};
