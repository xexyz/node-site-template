'use strict';

module.exports = function(sequelize, DataTypes) {
    var Account = sequelize.define('account', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        number:{
            type: DataTypes.STRING,
            notNull: true
        },
        clearingCode:{
            type: DataTypes.STRING,
            notNull: true
        }
    }, {
        classMethods: {
            associate: function(models) {
                Account.belongsTo(models.company, {
                    onDelete: "CASCADE"
                });
                Account.belongsToMany(models.user, {through: models.userAccount});
                Account.belongsToMany(models.product, {through: models.accountProduct});
            }
            
        }
    });

    return Account
};
