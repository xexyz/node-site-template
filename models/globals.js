'use strict';

module.exports = function(sequelize, DataTypes) {
    var Global = sequelize.define('globalSettings', {
        context : {
            type: DataTypes.STRING,
            notNull: true,
            primaryKey: true,
            unique: true
        },
        applyGlobalAddon:{
            type: DataTypes.BOOLEAN,
            notNull: true,
            default: false
        },
        globalViewOnly:{
            type: DataTypes.BOOLEAN,
            notNull: true,
            default: false
        },
        globalAddonValue:{
            type: DataTypes.INTEGER,
            notNull: true,
            default: 0
        },
    }, {
        classMethods: {

        }
    });

    return Global
};
