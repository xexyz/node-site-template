var models			 = require('../');

var drop = `DROP PROCEDURE IF EXISTS spGetInstrAddOns;`;

var query = `CREATE PROCEDURE \`spGetInstrAddOns\` (acompanyId INT, aINSTR_ID INT)
BEGIN
	DECLARE vINSTR_TYPE CHAR(1);

	SELECT	i.instrumentTypeCode
	INTO	vINSTR_TYPE
	FROM	instruments i
	WHERE	i.id = aINSTR_ID;

	DROP TABLE IF EXISTS tmp_month;
	CREATE TEMPORARY TABLE tmp_month (
	  month_code char(1),
	  month_no int(11));

	INSERT INTO tmp_month(month_code, month_no) VALUES('F',1),('G',2),('H',3),('J',4),('K',5),('M',6),('N',7),('Q',8),('U',9),('V',10),('X',11),('Z',12);

	DROP TABLE IF EXISTS tmp_zero_offset;
      
    CREATE TEMPORARY TABLE tmp_zero_offset AS (
	SELECT	t.productId, MIN((SUBSTRING(t.monthYearCode,2,2) * 12) + m.month_no) AS zero_offset
	FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.monthYearCode,1,1) = m.month_code
	WHERE	t.instrumentTypeCode = 'F' AND t.expiration >= CURDATE() 
	GROUP BY t.productId);

	# Using a normal table to allow it to be used more than once in the query
    # Use a UID name and prepare to allow for multiple concurrent usage

    SET @tmpName = concat('tio_', replace(uuid(), '-', ''));

    SET @cmd1 = replace(
	'CREATE TABLE table_name (INDEX tio1_idx(productId, monthYearCode)) AS (
	SELECT	t.productId, t.monthYearCode, SUBSTRING(t.monthYearCode,1,1) month_part, SUBSTRING(t.monthYearCode,2,2) year_part, (SUBSTRING(t.monthYearCode,2,2) * 12) + m.month_no AS year_month_num, ((SUBSTRING(t.monthYearCode,2,2) * 12) + m.month_no)  - zo.zero_offset AS instr_offset
	FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.monthYearCode,1,1) = m.month_code
            INNER JOIN tmp_zero_offset zo
            ON t.productId = zo.productId
	WHERE	t.instrumentTypeCode = ''F'' AND t.expiration >= CURDATE() 
	ORDER BY 1,4,3)', 'table_name', @tmpName);

    prepare stmt1 from @cmd1;
	execute stmt1;
	deallocate prepare stmt1;

    SET @param1 = acompanyId;
    SET @param2 = aINSTR_ID;

	if (vINSTR_TYPE = 'F') then

		SET @cmd2 = replace(
		'SELECT	cita.lowerSize,
				cita.upperSize,
				cita.addedTicks
		FROM	instruments i
				INNER JOIN table_name tio
				ON	tio.productId = i.productId
					AND tio.monthYearCode = i.monthYearCode
				LEFT OUTER JOIN companyinstrumenttypes cit
				ON	tio.productId = cit.productId
					AND tio.instr_offset = cit.timeOffset1
					AND cit.instrumentTypeCode = i.instrumentTypeCode
					AND cit.companyId = ?
				LEFT OUTER JOIN companyinstrumenttypes cit2
				ON	tio.productId = cit2.productId
					AND tio.month_part = cit2.anchorMonth1
					AND tio.instr_offset BETWEEN IFNULL(cit2.fromOffset, 0) AND IFNULL(cit2.toOffset, 100000000)
					AND cit2.instrumentTypeCode = i.instrumentTypeCode
					AND cit2.companyId = ?
				LEFT OUTER JOIN companyinstrumenttypeaddons cita
				ON	COALESCE(cit2.id, cit.id) = cita.id
		WHERE	i.id = ?
				AND i.expiration >= CURDATE()
				AND COALESCE(cit2.id, cit.id) IS NOT NULL
		ORDER BY 1', 'table_name', @tmpName);

		prepare stmt2 from @cmd2;
		execute stmt2 using @param1, @param1, @param2;
		deallocate prepare stmt2;

	else

		if (vINSTR_TYPE = 'S') then

			SET @cmd2 = replace(
			'SELECT	cita.lowerSize,
					cita.upperSize,
					cita.addedTicks
			FROM	instruments i
					INNER JOIN table_name tio
					ON	tio.productId = i.productId
						AND tio.monthYearCode = i.monthYearCode1
					INNER JOIN table_name tio2
					ON	tio2.productId = i.productId
						AND tio2.monthYearCode = i.monthYearCode2
					LEFT OUTER JOIN companyinstrumenttypes cit
					ON	cit.productId = i.productId
						AND tio.instr_offset = cit.timeOffset1
						AND tio2.instr_offset = cit.timeOffset2
						AND cit.instrumentTypeCode = i.instrumentTypeCode
						AND cit.companyId = ?
					LEFT OUTER JOIN companyinstrumenttypes cit2
					ON	cit2.productId = i.productId
						AND tio.month_part = cit2.anchorMonth1
						AND tio2.month_part = cit2.anchorMonth2
						AND tio2.instr_offset BETWEEN IFNULL(cit2.fromOffset, 0) AND IFNULL(cit2.toOffset, 100000000)
						AND cit2.instrumentTypeCode = i.instrumentTypeCode
						AND cit2.companyId = ?
					LEFT OUTER JOIN companyinstrumenttypeaddons cita
					ON	COALESCE(cit2.id, cit.id) = cita.id
			WHERE	i.id = ?
					AND i.expiration >= CURDATE()
					AND COALESCE(cit2.id, cit.id) IS NOT NULL
			ORDER BY 1', 'table_name', @tmpName);

			prepare stmt2 from @cmd2;
			execute stmt2 using @param1, @param1, @param2;
			deallocate prepare stmt2;
            
		elseif (vINSTR_TYPE = 'I') then

			SET @cmd2 = replace(
			'SELECT	cita.lowerSize,
					cita.upperSize,
					cita.addedTicks
			FROM	instruments i
					INNER JOIN products p
					ON i.productId = p.id
					INNER JOIN table_name tio
					ON	tio.productId = p.interprodParent1Id
						AND tio.monthYearCode = i.monthYearCode1
					INNER JOIN table_name tio2
					ON	tio2.productId = p.interprodParent2Id
						AND tio2.monthYearCode = i.monthYearCode2
					LEFT OUTER JOIN companyinstrumenttypes cit
					ON	i.productId = cit.productId
						AND tio.instr_offset = cit.timeOffset1
						AND cit.instrumentTypeCode = i.instrumentTypeCode
						AND cit.companyId = ?
					LEFT OUTER JOIN companyinstrumenttypes cit2
					ON	i.productId = cit2.productId
						AND tio.month_part = cit2.anchorMonth1
						AND tio.instr_offset BETWEEN IFNULL(cit2.fromOffset, 0) AND IFNULL(cit2.toOffset, 100000000)
						AND cit2.instrumentTypeCode = i.instrumentTypeCode
						AND cit2.companyId = ?
					LEFT OUTER JOIN companyinstrumenttypeaddons cita
					ON	COALESCE(cit2.id, cit.id) = cita.id
			WHERE	i.id = ?
					AND i.expiration >= CURDATE()
					AND COALESCE(cit2.id, cit.id) IS NOT NULL				
			ORDER BY 1', 'table_name', @tmpName);

			prepare stmt2 from @cmd2;
			execute stmt2 using @param1, @param1, @param2;
			deallocate prepare stmt2;
		
        elseif (vINSTR_TYPE = 'C') then

			SET @cmd2 = replace(
			'SELECT cita.lowerSize,
					cita.upperSize,
					cita.addedTicks
	        FROM	instruments i
					INNER JOIN products p
					ON i.productId = p.id   
					INNER JOIN table_name tio
					ON	tio.productId = p.interprodParent1Id
						AND tio.monthYearCode = i.monthYearCode1
					INNER JOIN table_name tio2
					ON	tio2.productId = p.interprodParent2Id
						AND tio2.monthYearCode = i.monthYearCode2
					LEFT OUTER JOIN companyinstrumenttypes cit
					ON	i.productId = cit.productId
						AND tio.instr_offset = cit.timeOffset1
						AND tio2.instr_offset = cit.timeOffset2
						AND cit.instrumentTypeCode = i.instrumentTypeCode
						AND cit.companyId = ?
					LEFT OUTER JOIN companyinstrumenttypes cit2
					ON	i.productId = cit2.productId
						AND tio.month_part = cit2.anchorMonth1
						AND tio2.month_part = cit2.anchorMonth2
						AND tio2.instr_offset BETWEEN IFNULL(cit2.fromOffset, 0) AND IFNULL(cit2.toOffset, 100000000)
						AND cit2.instrumentTypeCode = i.instrumentTypeCode
						AND cit2.companyId = ?
					LEFT OUTER JOIN companyinstrumenttypeaddons cita
					ON	COALESCE(cit2.id, cit.id) = cita.id
			WHERE	i.id=?
					AND i.expiration >= CURDATE()
					AND COALESCE(cit2.id, cit.id) IS NOT NULL
			ORDER BY 1', 'table_name', @tmpName);

			prepare stmt2 from @cmd2;
			execute stmt2 using @param1, @param1, @param2;
			deallocate prepare stmt2;
            
		end if;
	end if;

	SET @cmd3 = replace('DROP TABLE IF EXISTS table_name', 'table_name', @tmpName);
	prepare stmt3 from @cmd3;
	execute stmt3;
	deallocate prepare stmt3;

	DROP TABLE IF EXISTS tmp_month;
	DROP TABLE IF EXISTS tmp_zero_offset;

END`;


module.exports = function(sequelize){
    sequelize.query(drop)
        .spread(function(results, metadata) {
            sequelize.query(query)
                .spread(function(results, metadata) {

                });
        });
};