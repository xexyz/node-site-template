var models			 = require('../');

var drop = `DROP PROCEDURE IF EXISTS spGetUserInstrList;`;

var query = `CREATE PROCEDURE \`spGetUserInstrList\`(aCOMPANY_ID INT, aUSER_ID INT)
BEGIN
CALL spGetAllCompAddOns(aCOMPANY_ID); 
  DROP TABLE IF EXISTS user_result; 
  CREATE TEMPORARY TABLE user_result  AS ( 
	SELECT tr.\`instrument_id\`, tr.\`product_id\`, tr.\`month_year\`, tr.\`instrument_type_code\` FROM \`tmp_result\` tr  
	JOIN \`account_products\` ap 
		USING (\`product_id\`) 
	JOIN \`user_accounts\` ua 
		USING (\`account_id\`) 
	WHERE ua.\`user_id\` = aUSER_ID 
	GROUP BY tr.\`instrument_id\`);
  SELECT 'exchange' as type, '#' as parent, CONCAT(e.\`icon_class\`, ' fa-2x') as icon, e.\`exchange_name\` as id, e.\`exchange_name\` as text  
	  FROM \`user_result\` tr 
		 JOIN \`products\` p 
			USING (\`product_id\`) 
		 JOIN \`exchanges\` e 
			USING (\`exchange_id\`) 
		GROUP BY id; 
  SELECT 'product' as type, e.\`exchange_name\` as parent, CONCAT(p.\`icon_class\`, ' fa-lg') as icon, p.\`product_name\` as id, p.\`description\` as text  
	  FROM \`user_result\` tr 
		 JOIN \`products\` p 
			USING (\`product_id\`) 
		 JOIN \`exchanges\` e 
			USING (\`exchange_id\`) 
		GROUP BY id; 
  SELECT 'kind' as type,  CONCAT(p.\`product_name\`, '_', t.\`instrument_type_code\`) as id, p.\`product_name\` as parent, t.\`instrument_type_name\` as text  
	  FROM \`user_result\` tr 
		 JOIN \`products\` p  
			USING (\`product_id\`)  
		 JOIN \`instrument_types\` t  
			USING (\`instrument_type_code\`) 
		GROUP BY id; 
  SELECT CASE WHEN i.\`instrument_type_code\` = 'F' THEN CONCAT(p.\`product_name\`, i.\`month_year\`) 
				  WHEN i.\`instrument_type_code\` = 'S' THEN CONCAT(p.\`product_name\`, SUBSTRING(i.\`month_year\`,1,4), p.\`product_name\`, SUBSTRING(i.\`month_year\`,5,3)) 
				  ELSE CONCAT(SUBSTRING_INDEX(p.\`product_name\`,'-',1), SUBSTRING(i.\`month_year\`,1,4), SUBSTRING_INDEX(p.\`product_name\`,'-',-1), SUBSTRING(i.\`month_year\`,5,3)) END as text, 
				  i.\`instrument_id\` as id, CONCAT(p.\`product_name\`, '_', i.\`instrument_type_code\`) as parent, i.\`instrument_id\` as li_attr, i.\`sort_value\`
	  FROM \`user_result\` tr 
			 JOIN \`instruments\` i  
				USING (\`instrument_id\`) 
			 JOIN \`products\` p  
				ON (tr.\`product_id\` = p.\`product_id\`)  
			GROUP BY id 
			ORDER BY i.\`sort_value\`, id;
  DROP TABLE IF EXISTS user_result; 
  DROP TABLE IF EXISTS tmp_result; 
END`;


module.exports = function(sequelize){
    sequelize.query(drop)
        .spread(function(results, metadata) {
            sequelize.query(query)
                .spread(function(results, metadata) {

                });
        });
};