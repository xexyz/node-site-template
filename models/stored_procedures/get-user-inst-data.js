var models			 = require('../');

var drop = `DROP PROCEDURE IF EXISTS spGetUserInstrData;`;

var query = `CREATE PROCEDURE \`spGetUserInstrData\`()
BEGIN

	DECLARE var_global_addon INT DEFAULT 0;

	SELECT CASE WHEN apply_global_addon = 1 THEN global_addon_value ELSE 0 END
	INTO var_global_addon
	FROM global_settings;
	
	DROP TABLE IF EXISTS tmp_month;
	CREATE TEMPORARY TABLE tmp_month (
	  month_code char(1),
	  month_no int(11));

	INSERT INTO tmp_month(month_code, month_no) VALUES('F',1),('G',2),('H',3),('J',4),('K',5),('M',6),('N',7),('Q',8),('U',9),('V',10),('X',11),('Z',12);

	DROP TABLE IF EXISTS tmp_zero_offset;

    CREATE TEMPORARY TABLE tmp_zero_offset AS (
	SELECT	t.product_id, MIN((SUBSTRING(t.month_year,2,2) * 12) + m.month_no) AS zero_offset
	FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.month_year,1,1) = m.month_code
	WHERE	t.instrument_type_code = 'F' AND t.expiration >= CURDATE() 
	GROUP BY t.product_id);

    SET @tmpName = concat('tio_', replace(uuid(), '-', ''));

	# Using a normal table to allow it to be used more than once
    SET @cmd1 = replace(
	'CREATE TABLE table_name (INDEX tio1_idx(product_id, month_year)) AS (
	SELECT	t.product_id, t.month_year, SUBSTRING(t.month_year,1,1) month_part, SUBSTRING(t.month_year,2,2) year_part, (SUBSTRING(t.month_year,2,2) * 12) + m.month_no AS year_month_num, ((SUBSTRING(t.month_year,2,2) * 12) + m.month_no)  - zo.zero_offset AS instr_offset
	FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.month_year,1,1) = m.month_code
            INNER JOIN tmp_zero_offset zo
            ON t.product_id = zo.product_id
	WHERE	t.instrument_type_code = ''F'' AND t.expiration >= CURDATE() 
	ORDER BY 1,4,3)', 'table_name', @tmpName);

    prepare stmt1 from @cmd1;
	execute stmt1;
	deallocate prepare stmt1;

    SET @cmd2 = replace(
	'SELECT	ul.user_id,
			ul.instrument_id,
			e.exchange_name,
			p.product_name,
			it.instrument_type_name,
			i.month_year,
			ul.quantity,
			COALESCE(cita.added_ticks, 0) + ? as added_ticks,
			ul.row_number
	FROM	user_layouts ul 
			INNER JOIN users u
			ON ul.user_id = u.user_id 
			INNER JOIN instruments i
			ON ul.instrument_id = i.instrument_id 
			INNER JOIN instrument_types it
			ON i.instrument_type_code = it.instrument_type_code
			INNER JOIN products p
			ON i.product_id = p.product_id 
			INNER JOIN exchanges e
			ON p.exchange_id = e.exchange_id 
			INNER JOIN table_name tio
			ON	tio.product_id = i.product_id
				AND tio.month_year = i.month_year
			LEFT OUTER JOIN company_instrument_type cit
			ON	tio.product_id = cit.product_id
				AND tio.instr_offset = cit.time_offset_1
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type cit2
			ON	tio.product_id = cit2.product_id
				AND tio.month_part = cit2.anchor_month_1
				AND tio.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
				AND ul.quantity BETWEEN cita.lower_size AND COALESCE(cita.upper_size, 2147483647)
	WHERE	i.instrument_type_code = ''F''
    UNION
	SELECT	ul.user_id,
			ul.instrument_id,
			e.exchange_name,
			p.product_name,
			it.instrument_type_name,
			i.month_year,
			ul.quantity,
			COALESCE(cita.added_ticks, 0) + ?,
			ul.row_number
	FROM	user_layouts ul 
			INNER JOIN users u
			ON ul.user_id = u.user_id 
			INNER JOIN instruments i
			ON ul.instrument_id = i.instrument_id 
			INNER JOIN instrument_types it
			ON i.instrument_type_code = it.instrument_type_code
			INNER JOIN products p
			ON i.product_id = p.product_id 
			INNER JOIN exchanges e
			ON p.exchange_id = e.exchange_id 
			INNER JOIN table_name tio
			ON	tio.product_id = i.product_id
				AND tio.month_year = i.month_year1
			INNER JOIN table_name tio2
			ON	tio2.product_id = i.product_id
				AND tio2.month_year = i.month_year2
			LEFT OUTER JOIN company_instrument_type cit
			ON	cit.product_id = i.product_id
				AND tio.instr_offset = cit.time_offset_1
				AND tio2.instr_offset = cit.time_offset_2
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type cit2
			ON	cit2.product_id = i.product_id
				AND tio.month_part = cit2.anchor_month_1
				AND tio2.month_part = cit2.anchor_month_2
				AND tio2.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
				AND ul.quantity BETWEEN cita.lower_size AND COALESCE(cita.upper_size, 2147483647)
	WHERE	i.instrument_type_code = ''S''
	UNION
	SELECT	ul.user_id,
			ul.instrument_id,
			e.exchange_name,
			p.product_name,
			it.instrument_type_name,
			i.month_year,
			ul.quantity,
			COALESCE(cita.added_ticks, 0) + ?,
			ul.row_number
	FROM	user_layouts ul 
			INNER JOIN users u
			ON ul.user_id = u.user_id 
			INNER JOIN instruments i
			ON ul.instrument_id = i.instrument_id 
			INNER JOIN instrument_types it
			ON i.instrument_type_code = it.instrument_type_code
			INNER JOIN products p
			ON i.product_id = p.product_id 
			INNER JOIN exchanges e
			ON p.exchange_id = e.exchange_id 
			INNER JOIN table_name tio
			ON	tio.product_id = p.interprod_parent_id1
				AND tio.month_year = i.month_year1
			INNER JOIN table_name tio2
			ON	tio2.product_id = p.interprod_parent_id2
				AND tio2.month_year = i.month_year2
				AND tio2.instr_offset = tio.instr_offset
			LEFT OUTER JOIN company_instrument_type cit
			ON	i.product_id = cit.product_id
				AND tio.instr_offset = cit.time_offset_1
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type cit2
			ON	i.product_id = cit2.product_id
				AND tio.month_part = cit2.anchor_month_1
				AND tio.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
				AND ul.quantity BETWEEN cita.lower_size AND COALESCE(cita.upper_size, 2147483647)
	WHERE	i.instrument_type_code = ''I''
	UNION
	SELECT	ul.user_id,
			ul.instrument_id,
			e.exchange_name,
			p.product_name,
			it.instrument_type_name,
			i.month_year,
			ul.quantity,
			COALESCE(cita.added_ticks, 0) + ?,
			ul.row_number
	FROM	user_layouts ul 
			INNER JOIN users u
			ON ul.user_id = u.user_id 
			INNER JOIN instruments i
			ON ul.instrument_id = i.instrument_id 
			INNER JOIN instrument_types it
			ON i.instrument_type_code = it.instrument_type_code
			INNER JOIN products p
			ON i.product_id = p.product_id 
			INNER JOIN exchanges e
			ON p.exchange_id = e.exchange_id 
			INNER JOIN table_name tio
			ON	tio.product_id = p.interprod_parent_id1
				AND tio.month_year = i.month_year1
			INNER JOIN table_name tio2
			ON	tio2.product_id = p.interprod_parent_id2
				AND tio2.month_year = i.month_year2
				
			LEFT OUTER JOIN company_instrument_type cit
			ON	i.product_id = cit.product_id
				AND tio.instr_offset = cit.time_offset_1
                AND tio2.instr_offset = cit.time_offset_2
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type cit2
			ON	i.product_id = cit2.product_id
				AND tio.month_part = cit2.anchor_month_1
                AND tio2.month_part = cit2.anchor_month_2
				AND tio2.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = u.company_id
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
				AND ul.quantity BETWEEN cita.lower_size AND COALESCE(cita.upper_size, 2147483647)
	WHERE	i.instrument_type_code = ''C''
	ORDER BY 1, 9', 'table_name', @tmpName);

    SET @param = var_global_addon;

    prepare stmt2 from @cmd2;
	execute stmt2 using @param,@param,@param,@param;
	deallocate prepare stmt2;

    SET @cmd3 = replace('DROP TABLE IF EXISTS table_name', 'table_name', @tmpName);

    prepare stmt3 from @cmd3;
	execute stmt3;
	deallocate prepare stmt3;

	DROP TABLE IF EXISTS tmp_month;
	DROP TABLE IF EXISTS tmp_zero_offset;

END`;


module.exports = function(sequelize){
    sequelize.query(drop)
        .spread(function(results, metadata) {
            sequelize.query(query)
                .spread(function(results, metadata) {

                });
        });
};