var models			 = require('../');

var drop = `DROP PROCEDURE IF EXISTS spGetInstrumentOffsets;`;

var query = `CREATE PROCEDURE \`spGetInstrumentOffsets\`()
BEGIN

	DROP TABLE IF EXISTS tmp_month;
	CREATE TEMPORARY TABLE tmp_month (
	  month_code char(1),
	  month_no int(11));

	INSERT INTO tmp_month(month_code, month_no) VALUES('F',1),('G',2),('H',3),('J',4),('K',5),('M',6),('N',7),('Q',8),('U',9),('V',10),('X',11),('Z',12);

	DROP TABLE IF EXISTS tmp_zero_offset;
	CREATE TEMPORARY TABLE tmp_zero_offset AS (
	SELECT	t.product_id, MIN((SUBSTRING(t.month_year,2,2) * 12) + m.month_no) AS zero_offset
		FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.month_year,1,1) = m.month_code
	WHERE	t.instrument_type_code = 'F' AND t.expiration >= CURDATE() 
	GROUP BY t.product_id);

	SELECT	t.product_id, t.month_year, ((SUBSTRING(t.month_year,2,2) * 12) + m.month_no) - zo.zero_offset AS instr_offset
	FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.month_year,1,1) = m.month_code
            INNER JOIN tmp_zero_offset zo
            ON t.product_id = zo.product_id
	WHERE	t.instrument_type_code = 'F' AND t.expiration >= CURDATE() 
	ORDER BY 1,3;

	DROP TABLE IF EXISTS tmp_month;
	DROP TABLE IF EXISTS tmp_zero_offset;
END`;


module.exports = function(sequelize){
    sequelize.query(drop)
        .spread(function(results, metadata) {
            sequelize.query(query)
                .spread(function(results, metadata) {

                });
        });
};