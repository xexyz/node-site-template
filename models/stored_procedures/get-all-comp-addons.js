var models			 = require('../');

var drop = `DROP PROCEDURE IF EXISTS spGetAllCompAddOns;`;

var query = `CREATE PROCEDURE \`spGetAllCompAddOns\`(aCOMPANY_ID INT)
BEGIN

	DROP TABLE IF EXISTS tmp_month;
	CREATE TEMPORARY TABLE tmp_month (
	  month_code char(1),
	  month_no int(11));

	INSERT INTO tmp_month(month_code, month_no) VALUES('F',1),('G',2),('H',3),('J',4),('K',5),('M',6),('N',7),('Q',8),('U',9),('V',10),('X',11),('Z',12);

	DROP TABLE IF EXISTS tmp_zero_offset;
      
    CREATE TEMPORARY TABLE tmp_zero_offset AS (
	SELECT	t.product_id, MIN((SUBSTRING(t.month_year,2,2) * 12) + m.month_no) AS zero_offset
	FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.month_year,1,1) = m.month_code
	WHERE	t.instrument_type_code = 'F' AND t.expiration >= CURDATE() 
	GROUP BY t.product_id);

	DROP TABLE IF EXISTS tmp_instr_offset;
    
    SET @tmpName = concat('tio_', replace(uuid(), '-', ''));
    
    SET @cmd1 = replace(
	'CREATE TABLE table_name (INDEX tio1_idx(product_id, month_year)) AS (
	SELECT	t.product_id, t.month_year, SUBSTRING(t.month_year,1,1) month_part, SUBSTRING(t.month_year,2,2) year_part, (SUBSTRING(t.month_year,2,2) * 12) + m.month_no AS year_month_num, ((SUBSTRING(t.month_year,2,2) * 12) + m.month_no)  - zo.zero_offset AS instr_offset
	FROM	instruments t
			INNER JOIN tmp_month m
			ON SUBSTRING(t.month_year,1,1) = m.month_code
            INNER JOIN tmp_zero_offset zo
            ON t.product_id = zo.product_id
	WHERE	t.instrument_type_code = ''F'' AND t.expiration >= CURDATE() 
	ORDER BY 1,4,3)', 'table_name', @tmpName);
    
    prepare stmt1 from @cmd1;
	execute stmt1;
	deallocate prepare stmt1;

    SET @cmd2 = replace(
	'CREATE TEMPORARY TABLE tmp_result AS 
     SELECT	i.instrument_id,
			i.instrument_type_code,
			i.product_id,
			i.month_year,
			cita.company_instrument_type_id,
			cita.lower_size,
			cita.upper_size,
			cita.added_ticks
	FROM	instruments i
			INNER JOIN table_name tio
			ON	tio.product_id = i.product_id
				AND tio.month_year = i.month_year
			LEFT OUTER JOIN company_instrument_type cit
			ON	tio.product_id = cit.product_id
				AND tio.instr_offset = cit.time_offset_1
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = ?
			LEFT OUTER JOIN company_instrument_type cit2
			ON	tio.product_id = cit2.product_id
				AND tio.month_part = cit2.anchor_month_1
				AND tio.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = ?
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
	WHERE	i.instrument_type_code = ''F''
			AND i.expiration >= CURDATE()
			AND COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) IS NOT NULL
	UNION
	SELECT	i.instrument_id,
			i.instrument_type_code,
			i.product_id,
			i.month_year,
			cita.company_instrument_type_id,
			cita.lower_size,
			cita.upper_size,
			cita.added_ticks
	FROM	instruments i
			INNER JOIN table_name tio
			ON	tio.product_id = i.product_id
				AND tio.month_year = i.month_year1
			INNER JOIN table_name tio2
			ON	tio2.product_id = i.product_id
				AND tio2.month_year = i.month_year2
			LEFT OUTER JOIN company_instrument_type cit
			ON	cit.product_id = i.product_id
				AND tio.instr_offset = cit.time_offset_1
				AND tio2.instr_offset = cit.time_offset_2
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = ?
			LEFT OUTER JOIN company_instrument_type cit2
			ON	cit2.product_id = i.product_id
				AND tio.month_part = cit2.anchor_month_1
				AND tio2.month_part = cit2.anchor_month_2
				AND tio2.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = ?
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
	WHERE	i.instrument_type_code = ''S''
			AND i.expiration >= CURDATE()
			AND COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) IS NOT NULL
	UNION
    SELECT	i.instrument_id,
			i.instrument_type_code,
			i.product_id,
			i.month_year,
			cita.company_instrument_type_id,
			cita.lower_size,
			cita.upper_size,
			cita.added_ticks
	FROM	instruments i
			INNER JOIN products p
			ON i.product_id = p.product_id
			INNER JOIN table_name tio
			ON	tio.product_id = p.interprod_parent_id1
				AND tio.month_year = i.month_year1
			INNER JOIN table_name tio2
			ON	tio2.product_id = p.interprod_parent_id2
				AND tio2.month_year = i.month_year2
			LEFT OUTER JOIN company_instrument_type cit
			ON	i.product_id = cit.product_id
				AND tio.instr_offset = cit.time_offset_1
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = ?
			LEFT OUTER JOIN company_instrument_type cit2
			ON	i.product_id = cit2.product_id
				AND tio.month_part = cit2.anchor_month_1
				AND tio.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = ?
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
	WHERE	i.instrument_type_code = ''I''
			AND i.expiration >= CURDATE()
			AND COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) IS NOT NULL
    UNION
    SELECT	i.instrument_id,
			i.instrument_type_code,
			i.product_id,
			i.month_year,
			cita.company_instrument_type_id,
			cita.lower_size,
			cita.upper_size,
			cita.added_ticks
	FROM	instruments i
			INNER JOIN products p
			ON i.product_id = p.product_id   
			INNER JOIN table_name tio
			ON	tio.product_id = p.interprod_parent_id1
				AND tio.month_year = i.month_year1
			INNER JOIN table_name tio2
			ON	tio2.product_id = p.interprod_parent_id2
				AND tio2.month_year = i.month_year2
			LEFT OUTER JOIN company_instrument_type cit
			ON	i.product_id = cit.product_id
				AND tio.instr_offset = cit.time_offset_1
				AND tio2.instr_offset = cit.time_offset_2 -1
				AND cit.instrument_type_code = i.instrument_type_code
				AND cit.company_id = ?
			LEFT OUTER JOIN company_instrument_type cit2
			ON	i.product_id = cit2.product_id
				AND tio.month_part = cit2.anchor_month_1
				AND tio2.month_part = cit2.anchor_month_2
				AND tio2.instr_offset BETWEEN IFNULL(cit2.from_offset, 0) AND IFNULL(cit2.To_offset, 100000000)
				AND cit2.instrument_type_code = i.instrument_type_code
				AND cit2.company_id = ?
			LEFT OUTER JOIN company_instrument_type_addons cita
			ON	COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) = cita.company_instrument_type_id
	WHERE	i.instrument_type_code = ''C''
			AND i.expiration >= CURDATE()
			AND COALESCE(cit2.company_instrument_type_id, cit.company_instrument_type_id) IS NOT NULL
	ORDER BY 1,2,3,4;', 'table_name', @tmpName);

    SET @param = aCOMPANY_ID;

    prepare stmt2 from @cmd2;
    DROP TEMPORARY TABLE IF EXISTS tmp_result;
	execute stmt2 using @param,@param,@param,@param,@param,@param,@param,@param;
    SELECT * FROM tmp_result;
	deallocate prepare stmt2;

	
    
    SET @cmd3 = replace('DROP TABLE IF EXISTS table_name', 'table_name', @tmpName);

    prepare stmt3 from @cmd3;
	execute stmt3;
	deallocate prepare stmt3;

	DROP TABLE IF EXISTS tmp_month;
	DROP TABLE IF EXISTS tmp_instr_offset;
	DROP TABLE IF EXISTS tmp_zero_offset;


END`;


module.exports = function(sequelize){
    sequelize.query(drop)
        .spread(function(results, metadata) {
            sequelize.query(query)
                .spread(function(results, metadata) {

                });
        });
};