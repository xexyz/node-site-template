'use strict';

module.exports = function(sequelize, DataTypes) {
    var CompanyInstrumentType = sequelize.define('companyInstrumentType', {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            notNull: true
        },
        timeOffset1: {
            type: DataTypes.INTEGER
        },
        timeOffset2: {
            type: DataTypes.INTEGER
        },
        anchorMonth1: {
            type: DataTypes.STRING
        },
        anchorMonth2: {
            type: DataTypes.STRING
        },
        fromOffset: {
            type: DataTypes.INTEGER
        },
        toOffset: {
            type: DataTypes.INTEGER
        }
    }, {
        classMethods: {
            associate: function(models) {
                CompanyInstrumentType.belongsTo(models.product);
                CompanyInstrumentType.belongsTo(models.instrumentType);
                CompanyInstrumentType.belongsTo(models.company);
            }
        }
    });

    return CompanyInstrumentType
};
