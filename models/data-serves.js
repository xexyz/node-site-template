'use strict';

module.exports = function(sequelize, DataTypes) {
    var Serve = sequelize.define('serve', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING,
            unique: true
        },
        address: {
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function(models) {
                Serve.hasMany(models.conn)
            }
        }
    });

    return Serve
};
