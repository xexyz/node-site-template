'use strict';

module.exports = function(sequelize, DataTypes) {
    var UserHistoryType = sequelize.define('userHistoryType', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        name:{
            type: DataTypes.STRING
        },
        description:{
            type: DataTypes.STRING
        },
        rowClass:{
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function(models) {

            }
        }
    });

    return UserHistoryType
};
