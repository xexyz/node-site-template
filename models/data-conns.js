'use strict';

module.exports = function(sequelize, DataTypes) {
    var Conn = sequelize.define('conn', {
        name: {
            type: DataTypes.STRING
        },
        port: {
            type: DataTypes.INTEGER
        },
        isLive: {
            type: DataTypes.BOOLEAN
        },
        inUse: {
            type: DataTypes.BOOLEAN
        }
    }, {
        classMethods: {
            associate: function(models) {
                Conn.belongsTo(models.serve, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
            }
        }
    });

    return Conn
};
