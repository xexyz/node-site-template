'use strict';

module.exports = function(sequelize, DataTypes) {
    var Forgot = sequelize.define('forgot', {
        token: {
            type: DataTypes.STRING
        },
        validUntil: {
            type: DataTypes.DATE
        }
    }, {
        classMethods: {
            associate: function(models) {
                Forgot.belongsTo(models.user);
            }
        }
    });

    return Forgot
};