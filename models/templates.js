'use strict';

module.exports = function(sequelize, DataTypes) {
    var Template = sequelize.define('template', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        name:{
            type: DataTypes.STRING,
            unique: true,
            notNull: true
        },
        isDefault:{
            type: DataTypes.BOOLEAN,
            defaultValue: 0,
        }
    }, {
        classMethods: {
            associate: function(models) {
                Template.hasMany(models.templateInstrumentType);
            }
        }
    });

    return Template
};
