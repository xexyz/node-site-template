'use strict';

module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('user', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: DataTypes.STRING,
            notNull: true,
            unique: true
        },
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: {
            type: DataTypes.STRING
        },
        firstName: {
            type: DataTypes.STRING
        },
        lastName: {
            type: DataTypes.STRING
        },
        phoneNumber: {
            type: DataTypes.STRING
        },
        lastLogin: {
            type: DataTypes.DATE
        },
        authyId: {
            type: DataTypes.BIGINT(11)
        },
        isInternal: {
            type: DataTypes.BOOLEAN
        },
        viewOnly: {
            type: DataTypes.BOOLEAN
        },
        tfaValidUntil :{
            type: DataTypes.DATE
        }
    }, {
        classMethods: {
            associate: function(models) {
                User.belongsTo(models.company, {
                    onDelete: "CASCADE"
                });
                User.belongsToMany(models.account, {through: models.userAccount})
            }
        }
    });

    return User
};
