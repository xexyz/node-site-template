'use strict';

module.exports = function(sequelize, DataTypes) {
    var UserTab = sequelize.define('userTab', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        name:{
            type: DataTypes.STRING
        },
        sortValue:{
            type: DataTypes.INTEGER,
            notNull: true
        },
        isActive:{
            type: DataTypes.BOOLEAN,
            default: false
        }

    }, {
        classMethods: {
            associate: function(models) {
                UserTab.belongsTo(models.user);
                UserTab.hasMany(models.tabRow);
            }
        },
        hooks:{
            afterUpdate: function(instance, options){
                UserTab.update({isActive: false}, {where: {id: {$ne: instance.id}, userId: instance.userId}});
            }
        }
    });

    return UserTab
};
