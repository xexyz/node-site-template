'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');

var u_database		= global.cfg.db_database;
var u_user			= global.cfg.db_user;
var u_password		= global.cfg.db_password;
var u_sequelize     = global.cfg.db_sequelize;

var sequelize = new Sequelize(u_database, u_user, u_password, u_sequelize);

var db        = {};

fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return (file.indexOf('.') !== 0)
            && (file.indexOf('index') < 0)
            && (file.indexOf('sample') < 0)
            && (file.indexOf('static') < 0)
            && (file.indexOf('stored') < 0);
    })
    .forEach(function(file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
if (global.cfg.db_sync) {
    db.sequelize
        .sync({force: true})
        .then(function (err) {
            fs.readdirSync(__dirname + '/sample_data')
                .filter(function (file) {
                    return (file.indexOf('.') !== 0)
                        && (file.indexOf('sample') > -1);
                })
                .forEach(function (file) {
                    require(path.join(__dirname + '/sample_data', file))();
                });
        }, function (err) {
            logger.error(err);
        })
    ;

    fs.readdirSync(__dirname + '/stored_procedures')
        .filter(function (file) {
            return (file.indexOf('.') !== 0);
        })
        .forEach(function (file) {
            require(path.join(__dirname + '/stored_procedures', file))(db.sequelize);
        })
    ;
}

module.exports = db;