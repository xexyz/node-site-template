'use strict';

module.exports = function(sequelize, DataTypes) {
    var CompanyInstrumentTypeAddon = sequelize.define('companyInstrumentTypeAddon', {
        lowerSize: {
            type: DataTypes.INTEGER,
            notnull: true
        },
        upperSize: {
            type: DataTypes.INTEGER
        },
        addedTicks: {
            type: DataTypes.INTEGER,
            notnull: true
        },
    }, {
        classMethods: {
            associate: function(models) {
                CompanyInstrumentTypeAddon.belongsTo(models.companyInstrumentType);
            }
        }
    });

    return CompanyInstrumentTypeAddon
};
