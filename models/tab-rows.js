'use strict';

module.exports = function(sequelize, DataTypes) {
    var TabRow = sequelize.define('tabRow', {
        sortValue:{
            type: DataTypes.INTEGER,
            notNull: true
        },
        quantity:{
            type: DataTypes.INTEGER
        },
        accountId1:{
            type: DataTypes.INTEGER
        },
        accountId2:{
            type: DataTypes.INTEGER
        }
    }, {
        classMethods: {
            associate: function(models) {
                TabRow.belongsTo(models.userTab);
                TabRow.belongsTo(models.instrument);
            }
        }
    });

    return TabRow
};
