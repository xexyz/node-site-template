'use strict';

module.exports = function(sequelize, DataTypes) {
    var Role = sequelize.define('role', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        roleType:{
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function(models) {
                Role.hasOne(models.user);
            }
        }
    });

    return Role
};
