'use strict';

module.exports = function(sequelize, DataTypes) {
    var Exchange = sequelize.define('exchange', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        code: {
            type: DataTypes.STRING,
            unique: true
        },
        name: {
            type: DataTypes.TEXT
        },
        description:{
            type: DataTypes.TEXT
        },
        iconClass: {
            type: DataTypes.STRING
        },
        code: {
            type: DataTypes.STRING
        },
        isCrossExchange: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        }
    }, {
        classMethods: {
            associate: function(models) {
                Exchange.hasMany(models.product)
            }
        }
    });

    return Exchange
};
