var models			 = require('../');

module.exports = function(){
    models.company.bulkCreate([
        { companyName: 'COMPANY NAME
        { companyName: 'Awesome Inc.' }
    ]).then(function() {
        models.user.bulkCreate([
            {username: 'barfooz', companyId: 2, id: 1},
            {username: 'foo', companyId: 2, id: 2},
            {username: 'bar', password: 'bar', companyId: 2, id: 3},
            {
                username: 'ahernandez',
                email: 'EMAIL HERE',
                password: 'test',
                firstName: 'Andrew',
                lastName: 'Hernandez',
                phoneNumber: 'xxx',
                authyId: 1452574,
                roleId: 3,
                companyId: 1,
                id: 4
            },
            {
                username: 'ahernandez2',
                email: 'ahernandez.runme@previews.emailonacid.com',
                password: 'test',
                firstName: 'Andrew',
                lastName: 'Hernandez',
                phoneNumber: '16173090194',
                authyId: 1452574,
                roleId: 1,
                companyId: 1,
                id: 5
            }
        ]).then(function () {
            models.userTab.bulkCreate([
                { id: 1, name: 'Cool Tab', sortValue: 0, isActive: false, userId: 4 },
                { id: 2, name: 'Rad Tab', sortValue: 1, isActive: true, userId: 4}
            ]).then(function() {
                models.account.bulkCreate([
                    { id: 1, number: '43dfd', clearingCode: 666, companyId: 1 },
                    { id: 2, number: 'asdf432', clearingCode: 666, companyId: 1 },
                    { id: 3, number: 'acvvvw32', clearingCode: 666, companyId: 1 },
                ]).then(function() {
                    models.accountProduct.bulkCreate([
                        { accountId: 1, productId: 1 },
                        { accountId: 1, productId: 8 },
                        { accountId: 1, productId: 9 },
                        { accountId: 2, productId: 1 },
                        { accountId: 2, productId: 6 }
                    ]).then(function() {
                        models.userAccount.bulkCreate([
                            { accountId: 1, userId: 1 },
                            { accountId: 2, userId: 1 },
                            { accountId: 3, userId: 1 }
                        ]).then(function() {

                        })
                    })
                })
            })
        })
    })
};
