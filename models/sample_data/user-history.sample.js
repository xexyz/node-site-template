var models			 = require('../');

module.exports = function(){
    models.userHistoryType.bulkCreate([
        { id: 1, name: 'request', description: 'request for a market'},
        { id: 2, name: 'RFQ', description: 'request for quote'},
        { id: 3, name: 'Info', description: 'information for the user'},
        { id: 4, name: 'Quotes', description: 'quote sent to the user'},
        { id: 5, name: 'limit_change', description: 'change of limit'},
        { id: 6, name: 'Fills', description: 'fills'},
        { id: 7, name: 'Warn', description: 'warning'}
    ]).then(function() {

    })
};