'use strict';

module.exports = function(sequelize, DataTypes) {
    var StageType = sequelize.define('stageType', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        type:{
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function(models) {

            }
        }
    });

    return StageType
};
