'use strict';

module.exports = function(sequelize, DataTypes) {
    var Instrument = sequelize.define('instrument', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        monthYearCode: {
            type: DataTypes.STRING
        },
        expiration: {
            type: DataTypes.DATE
        },
        isDisabled: {
            type: DataTypes.BOOLEAN
        },
        sortValue: {
            type: DataTypes.DECIMAL(12, 6)
        },
        monthYearCode1: {
            type: DataTypes.STRING
        },
        monthYearCode2: {
            type: DataTypes.STRING
        }
    }, {
        classMethods: {
            associate: function(models) {
                Instrument.belongsTo(models.product, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
                Instrument.belongsTo(models.instrumentType);
                Instrument.hasOne(models.tabRow);
            }
        }
    });

    return Instrument
};
