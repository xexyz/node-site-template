var models			 = require('../');

module.exports = function(){
    models.role.bulkCreate([
        { roleType: 'user' },
        { roleType: 'admin' },
        { roleType: 'superadmin' }
    ]).then(function() {

    })
};