'use strict';

module.exports = function(sequelize, DataTypes) {
    var Company = sequelize.define('company', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        companyName:{
            type: DataTypes.STRING,
            unique: true,
            notNull: true
        },
        updatestamp:{
            type: DataTypes.INTEGER,
            defaultValue: 0,
            notNull: true
        }
    }, {
        classMethods: {
            associate: function(models) {
                Company.hasMany(models.user);
                Company.hasMany(models.account);
            }
        }
    });

    return Company
};
