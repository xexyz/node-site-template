'use strict';

module.exports = function(sequelize, DataTypes) {
    var AccountProduct = sequelize.define('accountProduct', {
        maximumSize:{
            type: DataTypes.INTEGER
        }
    }, {
        classMethods: {

        }
    });

    return AccountProduct
};
