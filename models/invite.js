'use strict';

module.exports = function(sequelize, DataTypes) {
    var Invite = sequelize.define('invite', {
        email: {
            type: DataTypes.STRING
        },
        token: {
            type: DataTypes.STRING
        },
        validUntil: {
            type: DataTypes.DATE
        }
    }, {
        classMethods: {
            associate: function(models) {
                Invite.belongsTo(models.company);
            }
        }
    });

    return Invite
};
