'use strict';

module.exports = function(sequelize, DataTypes) {
    var Broker = sequelize.define('broker', {
        id: {
            type: DataTypes.BIGINT(11),
            primaryKey: true,
            autoIncrement: true
        },
        broker_name:{
            type: DataTypes.STRING,
            unique: true,
            notNull: true
        },
        updatestamp:{
            type: DataTypes.INTEGER,
            defaultValue: 0,
            notNull: true
        }
    }, {
        classMethods: {

        }
    });

    return Broker
};
