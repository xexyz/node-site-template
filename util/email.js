//email.js
var aws         = require('aws-sdk');
var pug         = require('pug');
var path        = require('path');
var ses         = new aws.SES(global.cfg.aws_ses_config);

ses.logger      = logger;

var self = this;

self.sendEmail = function(address, type, options, next) {
    var template = require('./email_templates/' + type + '.json');
    var html = pug.renderFile(path.join(__dirname, 'email_templates/' + type + '.pug'), options);

    var params = {
        Destination: {
            BccAddresses: [],
            ToAddresses: [address]
        },
        Message: {
            Body: {
                Html: {
                    Data: html,
                    Charset: 'UTF-8'
                },
                Text: {
                    Data: template.text,
                    Charset: 'UTF-8'
                }
            },
            Subject: {
                Data: template.subject,
                Charset: 'UTF-8'
            }
        },
        Source: 'Support <no-reply@test.com>'
    }

    ses.sendEmail(params, function (err, data) {
        if (err) logger.error(err.stack);
        else logger.info(data);
        next(err, data);
    });
};

module.exports.sendEmail = self.sendEmail;
