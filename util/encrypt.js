// config/encrypt.js
var crypto 			= require('crypto');

module.exports = {

	options : {
		algorithm : 'aes-256-cbc',
		key : 'BBDA20DF21792057759F79DEA6B9A90F',
		iv :  'FE604ECC0131BCD7',
	},	
	
	encrypt : function(text){
		var cipher = crypto.createCipheriv(this.options.algorithm, new Buffer(this.options.key), new Buffer(this.options.iv));
		var crypted = cipher.update(new Buffer(text), 'utf8', 'hex')
		crypted += cipher.final('hex');

		return crypted;
	},
	
};

