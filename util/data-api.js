var Dict = require('collections/dict');

self = this;

var clientTypeOffset = 1;
var clientMessageType = [
// MESSAGES HERE
];

var serverTypeOffset = 1001;
var serverMessageType = [
// MESSAGES HERE
];

var messageFieldOffset = 1;
var messageField = [
// MESSAGES HERE
]

var errors = [
// ERRORS HERE
];

//setup
var clientTypeDict = new Dict();
for (var i = 0; i < clientMessageType.length; i++)
    clientTypeDict.add(i + clientTypeOffset, clientMessageType[i])

var serverTypeDict = new Dict();
for (var i = 0; i < serverMessageType.length; i++)
    serverTypeDict.add(i + serverTypeOffset, serverMessageType[i])

var messageFieldDict = new Dict();
for (var i = 0; i < messageField.length; i++)
    messageFieldDict.add(i + messageFieldOffset, messageField[i])


module.exports.fieldToString = function(num){
    if (num - messageFieldOffset >= messageField.length)
        return 'undefined';
    return messageField[num - messageFieldOffset];
};

module.exports.stringToField = function(string){
    return messageFieldDict.get(string);
};


module.exports.clientTypeToString = function(num){
    if (num - clientTypeOffset >= clientMessageType.length)
        return 'undefined';
    return clientMessageType[num - clientTypeOffset];
};

module.exports.stringToClientType = function(string){
    return clientTypeDict.get(string);
};


module.exports.serverTypeToString = function(num){
    if (num - serverTypeOffset >= serverMessageType.length)
        return 'undefined';
    return serverMessageType[num - serverTypeOffset];
};

module.exports.stringToServerType = function(string){
    return serverTypeDict.get(string);
};

module.exports.bid = 0;
module.exports.ask = 1;
