//util/user-history.js

var self = this;

self.log = function(id, type, message) {
    console.log(message);
    models.userHistory
        .create({
            userId : id,
            userHistoryTypeId: type,
            message: message,
            timestamp: moment.utc()
        }).then(function(tab){
            var data = {
                message: tab.message
            }
            return data;
        }, function(err){

        })
    ;
};

module.exports.log = self.log;