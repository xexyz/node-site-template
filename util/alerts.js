//alerts.js
var aws = require('aws-sdk');
var sns = new aws.SNS(global.cfg.aws_sns_config);
var template = require('./alert-templates.json');

sns.logger = logger;

this.sendMessage = function(type, options, next) {
    var params = {
        Message: template[type],
        // MessageAttributes: {
        //     someKey: {
        //         DataType: 'STRING_VALUE', /* required */
        //         BinaryValue: new Buffer('...') || 'STRING_VALUE',
        //         StringValue: 'STRING_VALUE'
        //     },
        //     /* anotherKey: ... */
        // },
        // MessageStructure: 'STRING_VALUE',
        Subject: 'Monitoring',
        TopicArn: global.cfg.aws_sns_topic_arn
    };

    // sns.publish(params, function(err, data) {
    //     if (err) logger.error(err.stack);
    //     else logger.info(data);
    //     next(err, data);
    // });
};


module.exports.send = this.sendMessage;
