//data-conns.js
var EverSocket 			= require('eversocket').EverSocket

var databuild             = require('../middlewares/data-builder')
var dataparse             = require('../middlewares/data-parser');

var pub = require('redis').createClient(global.cfg.redis_store_options);
var sub = require('redis').createClient(global.cfg.redis_store_options);
var errorSub = require('redis').createClient(global.cfg.redis_store_options);

var self = this;

var heartid;

// api socket =================================================================
self.socket = new EverSocket({
    reconnectWait: 5000,
    timeout: 8000,
    reconnectOnTimeout: true
});


self.socket.on('close', function() {
    // logger.log('info', 'Connection to API Closed', { 'ipAddress' : global.cfg.api_ip, 'port' : global.cfg.api_port});
    // if (data_api_connected){
    //     if  (app) {
    //         app.io.emit('api_status', 'bad');
    //         app.io.emit('notification', {'message' : 'The engine has become unavailable.', 'className': 'warn'} );
    //     }
    //     self.teardownApi();
    // }
});

self.socket.on('error', function(err){
    // logger.log('info', 'Connection to API Errored', { 'ipAddress' : global.cfg.api_ip, 'port' : global.cfg.api_port, 'error' : err});
    // if  (app)
    //     app.io.emit('api_status', 'bad');
    //teardownApi();
    //serverRestart();
});

self.socket.on('reconnect', function() {
    // logger.info('Reonnected to API', { 'ipAddress' : global.cfg.api_ip, 'port' : global.cfg.api_port});
    // self.setupApi();
});

self.heartbeat = function(){
    // LOGOUTHOUR = moment({h:15});
    // LOGOUTWARN = moment({h:14, m: 58});
    // var date = new moment();
    // if (date.isAfter(LOGOUTHOUR) && app && global.cfg.restrict_hours)
    //     app.io.in("L_1").emit('logout', {'message' : 'Logging out.', 'className': 'info'} );
    //
    // if (date.isAfter(LOGOUTWARN) && !logout_warned && app && global.cfg.restrict_hours){
    //     app.io.in("L_1").emit('notification', {'message' : 'Logging out all users in 2 minutes.', 'className': 'info'} );
    //     logout_warned = true;
    // }
    //

    var beat = {
        type: 'heartbeat',
        context_id: 'hb'
    };
    pub.publish('send_to_api', JSON.stringify(beat));
};

var line = '';
self.socket.on('data', function(data) {
    line += String(data);
    if (line.slice(-1) != '\n')
        return;

    var lines =  line.split('\n');
    line = '';
    for (var i = 0; i < lines.length; i++) {
        if (lines[i] === '')
            continue;

        dataparse(lines[i], function(err, data){
            pub.publish(data.type, JSON.stringify(data));
        })
    }
});

self.setup = function(){
    models.conn
        .findAll({where: {inUse: true}, include: [models.serve]})
        .then(function(conns){
            self.socket.connect(conns[0].port, conns[0].serve.address, function() {
                pub.publish('send_to_api', JSON.stringify(global.cfg.api_login))
                heartid = setInterval(self.heartbeat, 2500);
            });
        }, function(err){
            //done(err, null);
        });
};

self.sendToApi = function(data){
    databuild(data, function(err, str){
        self.socket.write(str);
        logger.info('api message sent', {message: str.slice(0, -1)});
    });
};

sub.subscribe('send_to_api');
sub.on("message", function (channel, message) {
    logger.info('api request received', {message: message.slice(0, -1)});
    self.sendToApi(JSON.parse(message));
});

errorSub.subscribe('error');
errorSub.on("message", function (channel, message) {
    logger.warn('api error', message);
});
module.exports = self.setup;
