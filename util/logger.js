var winston = require('winston'),

defaults = {},

// singleton
logger,

createLogger = function createLogger(options) {
  var opts;

  if (logger) {
    return logger;
  }

  if (global.cfg.is_test){
      logger = new (winston.Logger)({
          transports: [
              new (winston.transports.Console)({
                  level: 'error',
              })
          ]
      });
  }
  else{
      logger = new (winston.Logger)({
          transports: [
              new (winston.transports.Console)({
                  level: 'info',
              }),
              new (require('winston-daily-rotate-file'))({
                  filename: 'logs/debug-system.log',
                  datePattern: '.yyyy-MM-dd',
                  level : 'debug',
                  handleExceptions: true,
                  humanReadableUnhandledException: true,
                  json: true,
              })//,
              // new (require('winston-firehose'))({
              //     streamName: 'firehose_stream_name',
              //     firehoseOptions: {
              //         'region': 'us-east-1'
              //     }
              // })
              //todo setup this winston-firehose thing
          ]
      });
  }

  logger.exitOnError = false;
  return logger;
};

module.exports = createLogger;