var request = require('request');

module.exports = function(url, profile, callback){

    var endpoint = url + '-' + profile + '.json'
    request({
        url: endpoint,
        json: true
    }, function (err, response, body) {
        if (response.statusCode === 200) {
            callback(null, body);
        }
        callback(body.error, body);
    })
};