//controllers/login.js
var authy = require('authy')(global.cfg.authy_token,
    (!global.cfg.is_dev && 'http://sandbox-api.authy.com'));

var self = this;

/**
 * Entry point for the login routes.  If a req is authenticated (via passport) and the session contains a valid TFA,
 * then redirect to /home, otherwise render the login page.
 * @param req
 * @param res
 * @returns {*}
 */
self.index = function (req, res){
    if (req && req.isAuthenticated() && req.session.validTfa)
        return res.redirect('/home');

    res.render('login/login');
};

/**
 * Render the Two-Factor Auth page.  If a req is authenticated and contains a valid TFA, then redirect to /home.
 * If the req is not yet authenticated, redirect to /login, otherwise render the tfa page.
 * @param req
 * @param res
 * @returns {*}
 */
self.tfa = function (req, res){
    if (req && req.isAuthenticated() && req.session.validTfa)
        return res.redirect('/home');

    if (!req.isAuthenticated())
        return res.redirect('/login');

    res.render('login/tfa');
};


/**
 * First stage of authentication, sets most profile and session information.  Returns a redirect.
 * @param req
 * @param res
 */
self.auth1 = function(req, res) {
    if (!req.session.profile)
        req.session.profile = {};
    req.session.profile.username = req.user.username;
    req.session.profile.email = req.user.email;
    req.session.profile.roleId = req.user.roleId;
    req.session.profile.userId = req.user.userId;
    req.session.profile.companyName = req.user.company.companyName;
    req.session.profile.fullName = req.user.firstName + ' ' + req.user.lastName;

    req.session.isInternal = (req.user.isInternal == 1 ? true : false);
    req.session.viewOnly = (req.user.viewOnly == 1 ? true : false);
    req.session.userId = req.user.id;
    req.session.companyId = req.user.companyId
    req.session.roleId = req.user.roleId;
    req.session.authyId = req.user.authyId;
    req.session.validTfa = moment.utc().isBefore(moment(req.user.tfaValidUntil));

    //todo remove other active sessions

    logger.info('User found and authenticated, logging in');
    res.send({redirect: 'login/tfa'});
};

/**
 * Second stage of authentication.  Checks for valid TFA.
 * @param req
 * @param res
 */
self.auth2 = function(req, res){
    var id = req.session.authyId;
    var token = req.body.token;
    var rem = req.body.remember;

    authy.verify(id, token, function(err, data){
        if (err){
            return res.sendStatus(401);
        }
        req.session.validTfa = true;
        res.send({redirect: '/home'});
        if (rem){
            models.user
                .update({tfaValidUntil: moment.utc().add(30, 'd')}, {where: {id: req.session.userId}})
                .then(function(){})
        }
    });
};

module.exports.index = self.index;
module.exports.tfa = self.tfa;
module.exports.logout = self.logout;

module.exports.auth1 = self.auth1;
module.exports.auth2 = self.auth2;
