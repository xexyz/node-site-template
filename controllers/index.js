//controllers/index.js
var fs 				= require('fs');
//var alerts			= require('../util/alerts.js');

var self = this;

/**
 * No robots for the robots
 * @param req
 * @param res
 */
self.no_robots = function (req, res) {
	res.type('text/plain');
	res.send('User-agent: *\nDisallow: /');
};

/**
 * Redirect for the index path
 * @param req
 * @param res
 */
self.index = function (req, res){
    res.redirect('/login')
};

/**
 * Logout the user and destroy the session information.
 * @param req
 * @param res
 */
self.logout = function(req, res) {
    alerts.send('logout', {profile: req.session.profile});
    req.logout();
    logger.info('User logged out, destroying session', {'userName': req.session.username });
    req.session.destroy(function(err){
        res.redirect('/');
    });
};

module.exports.index = self.index;
module.exports.logout = self.logout
module.exports.no_robots = self.no_robots;
