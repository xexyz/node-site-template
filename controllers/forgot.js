//controllers/forgot.js
var email       = require('../util/email.js');
var crypto	= require('crypto');
var async       = require('async');

var self = this;

/**
 * Index for this route, renders the request page
 * @param req
 * @param res
 */
self.index = function(req, res) {
    res.render('forgot/forgot');
};

/**
 * Produces the token for a password reset and sends it to the user.  Finds the user in the db, creates the token,
 * saves the token, sends the email.
 * @param req
 * @param res
 */
self.request = function(req, res){
    var name = req.body.username;

    async.waterfall([
        function(done) { //get user
            models.user
                .find({where: {$or: [{username: name},{email: name}]}})
                .then(function(user){
                    if (!user)
                        done('User not found');
                    else
                        done(null, user)
                })
        },
        function(user, done) { //create token
            crypto.randomBytes(20, function(err, buf) {
                var token = buf.toString('hex');
                done(err, token, user);
            });
        },
        function(token, user, done) {
            models.forgot
                .create({
                    userId: user.id,
                    token: token,
                    validUntil: moment.utc().add(global.cfg.token_valid_time, 'h')
                })
                .then(function () {
                    done(null, token, user)
                }, function(err){
                    done(err);
                });
        },
        function(token, user, done){
            var options = {
                link: req.headers.host + '/forgot/' + token
            };
            email.sendEmail(user.email, 'pass-request', options, function(err, data){
                done(err, data);
            });
        }
    ], function(err, result) {
        if (err) {
            logger.error('Error sending invite', {'error' : err} );
            res.sendStatus(204);
        }
        else {
            res.sendStatus(204);
        }
    });
};

/**
 * Renders the reset page.  First checks that the reset token is valid.
 * @param req
 * @param res
 * @returns {*}
 */
self.reset = function (req, res){
    if (!req.params.token)
        return res.redirect('/');

    var token = req.params.token;
    models.forgot
        .find({where: {
            token: token,
            validUntil: {$gt: moment.utc()}
        }})
        .then(function(forgot){
            if (!forgot) {
                return res.render('forgot/bad-token');
            }

            res.render('forgot/reset');
        }, function(err){
            logger.error(err);
            res.redirect('/');
        });
};

/**
 * Resets the password based on user spec.  First finds the user via the token, sets the password, sends an email,
 * destroys the token.
 * @param req
 * @param res
 */
self.set = function (req, res){
    var token = req.params.token;
    var pass = req.body.password;

    async.waterfall([
        function(done) { //get user
            models.forgot
                .find({where: {
                    token: token,
                    validUntil: {$gt: moment.utc()}
                }, include: [models.user]})
                .then(function(forgot){
                    if (!forgot)
                        done('Token not found');
                    else{
                        done(null, forgot)
                    }

                })
        },
        function(forgot, done) {
            forgot.user.password = pass;
            forgot.user.save()
                .then(function(){
                    done(null, forgot)
                })
            ;
        },
        function(forgot, done){
            var options = {
                link: req.headers.host + '/login'
            };
            email.sendEmail(forgot.user.email, 'pass-reset', options, function(err, data){
                forgot.destroy();
                done(err, data);
            });
        }
    ], function(err, result) {
        if (err) {
            logger.error('Error reseting password', {'error' : err} );
            res.sendStatus(204);
        }
        else {
            res.sendStatus(204);
        }
    });
};

module.exports.index = self.index;
module.exports.request = self.request;
module.exports.reset = self.reset;
module.exports.set = self.set;
