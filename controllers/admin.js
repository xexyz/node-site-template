var email       = require('../util/email.js')
var self = this;

self.index = function (req, res){
    res.redirect('/admin/status');
};

self.status = function(req, res){
    res.render('admin/status');
};

self.activity = function(req, res){
    res.render('admin/activity');
};

self.invite = function(req, res){
    res.render('admin/invite');
};

self.sendInvite = function(req, res){
    console.log(req.body);
};

module.exports.index = self.index;
module.exports.status = self.status;
module.exports.activity = self.activity;
module.exports.invite = self.invite;

module.exports.sendInvite = self.sendInvite;
