//controllers/home-io.js
var self = this;
var app;
var api;

var pub = require('redis').createClient(global.cfg.redis_store_options);
var sub = require('redis').createClient(global.cfg.redis_store_options);

self.index = function (req, res){
    res.status(200);
};

self.subscribe = function(req, res){
    var data = req.data;
    var contextId = req.session.companyId + '_' + req.session.userId + '_' + data.row_id;
    var mess = {
        companyId: req.session.companyId,
        userId: req.session.userId,
        rowId: data.row_id,
        contextId: contextId,
    }

    req.socket.join(contextId);

    pub.publish('subscribe', JSON.stringify(mess));
    res.status(200);
};

self.unsubscribe = function(req, res){
    var data = req.data;
    // pub.publish('unsubscribe', JSON.stringify(mess));
    res.status(200);
};

self.action = function(req, res) {
    //DO SOMETHING HERE
};


//latency monitoring
self.ping = function(){
    //latentTime = +new Date();
    app.io.emit('ping');
};

self.pong = function(req, res) {
    latency = +new Date() - latentTime;

    var uid = req.session.user_id;
    if (uid in l_metrics){
        if (l_metrics[uid].samples.length >= sam_max)
            l_metrics[uid].samples.shift();

        l_metrics[uid].samples.push(latency);

        var avg_lat = 0;
        for(var i = 0; i < l_metrics[uid].samples.length; i++)
            avg_lat += l_metrics[uid].samples[i];

        avg_lat /= l_metrics[uid].samples.length;

        var cat_lat = 0;
        if (avg_lat > l_metrics[uid].lower_band && avg_lat < l_metrics[uid].upper_band )
            cat_lat = 1;
        else if (avg_lat > l_metrics[uid].upper_band)
            cat_lat = 2;

        var data = { 'category': cat_lat };
        var avg = parseInt(avg_lat);
        if (req.session.is_internal){
            data['average'] = avg;
            data['current'] = latency;
        }

        res.status(200).json(data);
    }
    else if (uid != undefined){
        db.client.query("SELECT latency_lower_band as llb, latency_upper_band as lup \
	  		FROM users WHERE user_id=? LIMIT 1", [uid], function(err, rows) {

            if (!rows)
                return;

            l_metrics[uid] = {
                lower_band: rows[0].llb,
                upper_band: rows[0].lup,
                samples: [latency],
            };
            logger.info('added user to latency monitor', {'userName': req.session.username, 'metrics' : l_metrics[uid] });
        });
    }
};

// events =======================================================================================================
sub.subscribe('heartbeat');
sub.subscribe('OTHER MESSAGES HERE');

sub.on("message", function (channel, message) {
    var data=  JSON.parse(message);
    switch(channel) {
        case 'heartbeat':
            var mess = {
                timestamp: moment.utc()
            }
            app.io.emit('health', mess);
            break;
        case 'OTHER MESSAGE HERE':
	    //DO STUFF

            app.io.in(data.contextId).emit('RESPONSE', mess);
            break;

    }
});

module.exports = function(_app, _api) {
    app = _app;
    api = _api;
    app.io.route('home/hello',        self.index);
    app.io.route('home/subscribe',    self.subscribe);
    app.io.route('home/unsubscribe',  self.unsubscribe);
    app.io.route('home/pong',         self.pong);
    var pingId = setInterval(self.ping, 1000);
}
