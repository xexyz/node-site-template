//settings.js
var self = this;

self.index = function(req, res){
    res.redirect('/settings/profile')
}

self.profile = function(req, res){
    models.user
        .find({where: { id: req.session.userId }, include: [models.company]})
        .then(function(user) {
            res.render('settings/profile', {user : user});
            logger.info('Profile requested, info sent', {'userName' : req.session.username, 'data': data});
	    })
    ;
};

self.updateProfile = function(req, res){
  //DO STUFF
};

self.accounts = function(req, res){
    // DO STUFF
    res.render('settings/accounts');
}

self.permissions = function(req, res){
    res.render('settings/permissions');
}

self.notifications = function(req, res){
    res.render('settings/notifications');
}

self.security = function(req, res){
    res.render('settings/security');
}


module.exports.index = self.index;
module.exports.profile = self.profile;
module.exports.permissions = self.permissions;
module.exports.notifications = self.notifications;
module.exports.security = self.security;
