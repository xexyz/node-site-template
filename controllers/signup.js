//controllers/signup.js
var authy = require('authy')(global.cfg.authy_token,
    (!global.cfg.is_dev && 'http://sandbox-api.authy.com'));

var self = this;

self.index = function(req, res){
    res.redirect('/');
};

self.token = function (req, res){
    if (!req.params.token)
        return res.redirect('/');

    var token = req.params.token;
    models.invite
        .find({where: {
                token: token,
                validUntil: {$gt: moment.utc()}
              }})
        .then(function(invite){
            if (!invite) {
                return res.render('signup/bad-token');
            }

            res.render('signup/signup', {email: invite.email});
        }, function(err){
            logger.error(err);
            res.redirect('/');
        });
};


module.exports.index = self.index;
