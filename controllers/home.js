//home.js
var async	    = require('async');
var pug             = require('pug');
var os              = require('os');
var uhis            = require('../util/user-history.js');
var self = this;

self.index = function(req, res){
	res.render('home/home', {ip: os.hostname()});
}

self.requestprice = function(req, res){
    var instId = req.params.instId;

	models.instrument
        .find({where: {id: instId}, include: [models.product]})
        .then(function(instrument) {
			var quantity = (req.query.quantity === undefined ? instrument.product.minSize : req.query.quantity);
			var rowName  = instId + "_" + quantity;

            var instrumentName;
            var n1 = instrument.monthYearCode1;
            var n2 = instrument.monthYearCode2;
            var p = instrument.product.name.split('-');
            switch(instrument.instrumentTypeCode){
                case 'F':
                    instrumentName = p[0] + n1;
                    break;
                case 'S':
                    instrumentName = p[0] + n1 + '-' + p[0] + n2;
                    break;
                default:
                    instrumentName = p[0] + n1 + '-' + p[1] + n2;
                    break;
            }

			var apiContext = instrumentName + "_" + quantity;
			var ip = instrument.instrumentTypeCode === 'C';

            var data = {
                'quantity': quantity,
                'row_name': rowName,
                'instrument': instrumentName,
                'instrument_id': instId,
                'multiplier': instrument.product.multiplier,
                'decimal_places': instrument.product.decimalPlaces,
                'api_context': apiContext,
                'is_interproduct': ip,
                'is_expired': moment.utc() > moment.utc(instrument.expiration),
                'is_view_only': instrument.isDisabled
            };
            res.send(data);
            logger.info('Instrument_id requested', {'username': req.session.username, 'instrument' : instrumentName, 'data':data});
        })
    ;
}

//generate the instrument grid
self.instruments = function( req, res ) {
    models.instrument
        .findAll({include:[{model: models.product, include:[models.exchange]}, models.instrumentType]})
        .then(function(ist) {
            var tree = {};
            for (i in ist) {
                var ex = ist[i].product.exchange.name;
                if (tree[ex] === undefined){
                    tree[ex] = {};
                    tree[ex].tab = ex;
                    tree[ex].itype = {};
                }
                var ty = ist[i].instrumentType.name;
                if (tree[ex].itype[ty] === undefined){
                    tree[ex].itype[ty] = {}
                    tree[ex].itype[ty].tab = ex + '/' + ty;
                    tree[ex].itype[ty].products = {};
                }
                var pd = ist[i].product.name;
                if (tree[ex].itype[ty].products[pd] === undefined){
                    tree[ex].itype[ty].products[pd] = {};
                    tree[ex].itype[ty].products[pd].tab = ex + '/' + ty + '/' + pd;
                    tree[ex].itype[ty].products[pd].inst = []
                }
                tree[ex].itype[ty].products[pd].inst.push(ist[i]);
            }

            var html = pug.renderFile('views/home/templates/instruments.pug', {tree: tree});
            data = {
                "success": true,
                "data": html
            }
            res.send(data);
        })
    ;

};

self.tabs = function(req, res){
    models.userTab
        .findAll({where: {userId: req.session.userId}})
        .then(function(tabs) {
            var html = pug.renderFile('views/home/templates/tabs.pug', {tabs: tabs});
            var data = {
                'success': true,
                'data': html
            }
            res.send(data);
        })
    ;
};

self.rows = function(req, res){
    var tabId = req.params.tabId;
    models.tabRow
        .findAll({where: { userTabId: tabId }, order: 'sortValue ASC'})
        .then(function(rows) {
            renRows = []
            for (row in rows)
                renRows.push({
                    id: rows[row].instrumentId,
                    quantity: rows[row].quantity,
                });
            var html = pug.renderFile('views/home/templates/grid.pug');
            var data = {
                'success': true,
                'data': html,
                'rows': renRows
            }
            res.send(data);
        })
        .then(function(){
            models.userTab
                .update({isActive: true}, {individualHooks: true, where: {id: tabId}})
                .then(function(){})
        })
    ;
};

self.savetab = function(req, res){
    var tabId = req.params.tabId;
    var rows = req.body.rows;

    models.tabRow
        .destroy({where: { userTabId: tabId }})
        .then(function() {
            return models.tabRow
                .bulkCreate(rows)
        })
        .then(function(){
            res.sendStatus(204);
        })
    ;
};

self.newtab = function(req, res){
    models.userTab
        .create({   name: 'New Tab',
                    userId: req.session.userId,
                    isActive: false })
        .then(function(tab) {
            var menu = pug.renderFile('views/home/templates/tab-menu.pug', {id: tab.id});
            var content = pug.renderFile('views/home/templates/tab-content.pug', {id: tab.id});
            var data = {
                'success': true,
                'menu': menu,
                'content': content
            };
            res.send(data);
        })
    ;
};



module.exports.index = self.index;
module.exports.requestsub = self.requestprice;
module.exports.instruments = self.instruments;
module.exports.tabs = self.tabs;
module.exports.rows = self.rows;
module.exports.newtab = self.newtab;
module.exports.savetab = self.savetab;

