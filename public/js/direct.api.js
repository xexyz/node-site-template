$.fn.api.settings.api = {
    'send invite' : '/admin/invite',
    'send sinvite' : '/superadmin/invite',
    'login' : '/login',
    'tfa' : '/login/tfa',
    'request reset' : '/forgot',
    'reset password' : '/forgot/{token}',
    'req subscribe' : '/home/requestsub/{id}',
    'instrument grid' : '/home/instruments',
    'instrument tree' : '/explorer/instruments',
    'tabs' : '/home/tabs',
    'tab' : '/home/tabs/{tab}',
    'history table' : '/history/history',
    'rfq' : '/home/rfq',
    'cancel rfq' : '/home/crfq',
    'instrument details' : '/explorer/details/{id}',
    'instrument add' : '/explorer/add'
    // 'create user'   : '/create',
    // 'add user'      : '/add/{id}',
    // 'follow user'   : '/follow/{id}',
    // 'search'        : '/search/?query={value}'
};

// $.site('disable console'); // disables window.console functionality
// $.site('enable console'); // re-enables window.console functionality
//
// // enable all debug
//$.site('enable debug');
//
// // disable all debug
// $.site('disable debug');
//
// // selectively enable debug with second parameter
// $.site('enable debug', ['tab', 'dropdown']);
//
// // selectively disable debug with second parameter
// $.site('disable debug', ['tab', 'dropdown']);
//
// // modify one setting across all modules
// $.site('change setting', 'foo', 'bar');
//
// // modify multiple settings across all modules
// $.site('change settings', { foo: 'one', bar: 'two'} );
//
// // modify setting for selected modules
// $.site('change setting', 'foo', 'bar', ['popup', 'dropdown']);
//
// // modify multiple settings for selected modules
// $.site('change settings', { foo: 'one', bar: 'two' }, ['popup', 'dropdown']);
//
// // returns array of all enabled modules
// $.site('enabled modules');
//
// // returns array of all disabled modules
// $.site('disabled modules');
//
// // returns boolean whether module with name exists on site
// $.site('module exists', 'name');
//
// // normalizes console and requestAnimationFrame DOM methods
// $.site('normalize');
