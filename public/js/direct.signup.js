$('#profile').form({
    fields: {
        username: {
            identifier: 'username',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Username required'
                }
            ]
        },
        firstname: {
            identifier: 'firstname',
            rules: [
                {
                    type: 'empty',
                    prompt: 'First name required'
                }
            ]
        },
        lastname: {
            identifier: 'lastname',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Last name required'
                }
            ]
        },
        password: {
            identifier: 'password',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Password required'
                }
            ]
        },
        confirm: {
            identifier: 'confirm',
            rules: [
                {
                    type: 'match[password]',
                    prompt: 'Password fields do not match'
                }
            ]
        }
    },
    inline: true,
    onSuccess: function(event, fields){

        

    }
});

$('#tfa').form({
    fields: {
        phone: {
            identifier: 'phone',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Cellphone required'
                },
                {
                    type: 'regExp[^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$]',
                    prompt: 'Number not in correct format'
                }
            ]
        }
    },
    inline: true,
    onSuccess: function(event, fields){



    }
});

$('.ui.large.form').submit(function(e){
    e.preventDefault();
    return false;
});
