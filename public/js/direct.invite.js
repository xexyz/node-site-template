$('.ui.large.form').form({
    fields: {
        email: {
            identifier: 'email',
            rules: [
                {
                    type: 'empty',
                    prompt: 'E-mail required'
                },
                {
                    type   : 'email',
                    prompt : 'Please enter a valid e-mail'
                }
            ]
        }
    },
    onSuccess: function(event, fields){
        $('.ui.large.form').api({
            action: 'send invite',
            method: 'POST',
            serializeForm: true,
        })
    },
    onFailure: function(formErrors, fields){

    }
});

$('.ui.large.form').submit(function(e){
    e.preventDefault();
    return false;
});
