$('.ui.large.form').form({
    fields: {
        password: {
            identifier: 'password',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Password required'
                }
            ]
        },
        confirm: {
            identifier: 'confirm',
            rules: [
                {
                    type: 'match[password]',
                    prompt: 'Password fields do not match'
                }
            ]
        }
    },
    inline: true,
    onSuccess: function(event, fields){

        $('.ui.large.form').api({
            url: window.location.pathname,
            method: 'POST',
            serializeForm: true,
            onSuccess: function(res) {
                if ($('.ui.green.message').hasClass('hidden'))
                    $('.ui.green.message').transition('scale');
                window.setTimeout(function(){
                    window.location = '/'
                }, 3000);
            }
        })
    }
});

$('.ui.large.form').submit(function(e){
    e.preventDefault();
    return false;
});
