var remember = $.cookie('remember');
if ( remember == 'true' ) {
	var username = $.cookie('username');
	// autofill the fields
	$('#username').val(username);
	$('#remember').attr("checked", true);
}

$('.ui.large.form').form({
    fields: {
        username: {
            identifier: 'username',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Username or email required'
                }
            ]
        },
        password: {
            identifier: 'password',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Password required'
                }
            ]
        }
    },
    inline: true,
    onSuccess: function(event, fields){
        if ($('#remember').prop('checked') ) {
            var username = $('#username').val();
            // set cookies to expire in 14 days
            $.cookie('username', username, { expires: 14 });
            $.cookie('remember', true, { expires: 14 });
        } else {
            $.cookie('username', null);
            $.cookie('remember', null);
        }

        $('.ui.large.form').api({
            action: 'login',
            method: 'POST',
            serializeForm: true,
            onSuccess: function(res) {
                window.location = res.redirect
            },
            onFailure: function() {
                if ($('.ui.red.message').hasClass('hidden'))
                    $('.ui.red.message').transition('scale');
            }
        })
    }
});

$('.ui.large.form').submit(function(e){
    e.preventDefault();
    return false;
});
