$('.ui.large.form').form({
    fields: {
        username: {
            identifier: 'username',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Username or email required'
                }
            ]
        }
    },
    inline: true,
    onSuccess: function(event, fields){

        $('.ui.large.form').api({
            action: 'request reset',
            method: 'POST',
            serializeForm: true,
            onSuccess: function(res) {
                if ($('.ui.green.message').hasClass('hidden'))
                    $('.ui.green.message').transition('scale');
            }
        })
    }
});

$('.ui.large.form').submit(function(e){
    e.preventDefault();
    return false;
});
