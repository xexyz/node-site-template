$('.ui.modal')
    .modal({
        inverted: true
    })
    .modal('attach events', '#company', 'show')
;