$('.ui.large.form').form({
    fields: {
        token: {
            identifier: 'token',
            rules: [
                {
                    type: 'empty',
                    prompt: 'Token required.'
                }
            ]
        }
    },
    inline: true,
    onSuccess: function(event, fields){
        $('.ui.large.form').api({
            action: 'tfa',
            method: 'POST',
            serializeForm: true,
            onSuccess: function(res) {
                window.location = res.redirect
            },
            onFailure: function(response) {
                if ($('.ui.red.message').hasClass('hidden'))
                    $('.ui.red.message').transition('scale');
            }
        })
    }
});

$('.ui.large.form').submit(function(e){
    e.preventDefault();
    return false;
});
