var socket;
var table;
var quote_rows;
var is_loaded;
var remove_row;

// Set up ===============================================================
ko.observableArray.fn.subscribeArrayChanged = function(addCallback, deleteCallback) {
    var previousValue = undefined;
    this.subscribe(function(_previousValue) {
        previousValue = _previousValue.slice(0);
    }, undefined, 'beforeChange');
    this.subscribe(function(latestValue) {
        var editScript = ko.utils.compareArrays(previousValue, latestValue);
        for (var i = 0, j = editScript.length; i < j; i++) {
            switch (editScript[i].status) {
                case "retained":
                    break;
                case "deleted":
                    if (deleteCallback)
                        deleteCallback(editScript[i].value);
                    break;
                case "added":
                    if (addCallback)
                        addCallback(editScript[i].value);
                    break;
            }
        }
        previousValue = undefined;
    });
};

// Datatables ===========================
var set_table = function() {
    table = $('#quotetable').DataTable({
        searching: false,
        info: false,
        lengthChange: false,
        autoWidth: false,
        paging: false,
        responsive: true,
        order: [[1, 'desc']],
        columns: [
            {data: 'id', visible: false},
            {data: 'row_order', visible: false},
            {data: 'product_cell()', visible: true, className: 'qt-product'},
            {data: 'instrument_cell()', visible: true, className: 'qt-name'},
            {data: 'last_update_cell()', visible: true, className: 'qt-last'},
            {data: 'quantity_cell()', visible: true, className: 'qt-qty'},
            {data: 'bid_price_cell()', visible: true, className: 'qt-bid'},
            {data: 'bid_limit_cell()', visible: false, className: 'qt-bid-limit'},
            {data: 'ask_limit_cell()', visible: false, className: 'qt-ask-limit'},
            {data: 'ask_price_cell()', visible: true, className: 'qt-ask'},
            {data: 'controls_cell()', visible: true, className: 'qt-controls'},
            {data: 'remove_cell()', visible: true}
        ],
        language: {
            "emptyTable": "<div align='center'>To begin, click the Add Instrument button\.</div>"
        },
    });
};

var saveLayout = function(){
    $('#layout').html('Layout: <i class="notched circle loading icon"></i>')
    var rows = [];
    var cTab = $('.item.gridtab.active').attr('data-id');
    table.column( 0 )
        .data()
        .each( function ( value, index ) {
            var match = ko.utils.arrayFirst(quote_rows(), function(item) {
                return value === item.id;
            });

            rows.push({ 'sortValue' : index,
                'instrumentId' : match.instrument_id,
                'quantity' : match.quantity(),
                'userTabId' : cTab});
        } );

    $('#layout')
        .api({
            action: 'tab',
            method: 'POST',
            on: 'now',
            urlData :{
                tab : cTab
            },
            data :{
                rows: rows
            },
            onSuccess: function(res) {
               $(this).html('Layout: Saved');
            }
        })
    ;
};

// Quote rows ==============================
quote_rows = ko.mapping.fromJS( [] );
quote_rows.subscribeArrayChanged(
    function ( addedItem ) {
        addedItem.set_row_data(table.row.add( addedItem ).draw().node());
        addedItem.remove_cb = remove_row;
        addedItem.dtable_row = table.row($('#' + addedItem.id));
    },
    function ( deletedItem ) {
        table.row($('#' + deletedItem.id)).remove().draw();
    }
);

remove_row = function(row){
    row.removing = true;
    var dl = 0;
    var h = (table.rows().data().length == 1 ? 27 : 0);
    if (row.is_rfo() || row.is_rfb()) {
        row.cancel_rfq();
        dl = 200;
    }

    var matches = ko.utils.arrayFilter(quote_rows(), function (item) {
        return row.row_name === item.row_name;
    });

    var started = false;
    $('#' + row.id).removeClass('qt-row').find('td')
        .wrapInner('<div style="display: block;" />')
        .parent()
        .find('td > div')
        .stop(true, true).delay(dl)
        .animate({'height': h}, 500, function () {
            if (!started) {
                started = true;
                quote_rows.remove(function (item) {
                    return item.id == row.id
                });
                socket.emit('home/unsubscribe', {
                    'instrument': row.api_name,
                    'api_context': row.row_name,
                    'can_leave': matches <= 1,
                });
                saveLayout();
            }
        });
};

add_row = function (data){
    var rowId = makeId();
    quote_rows.push(new QuoteRow(data, rowId));
    socket.emit('home/subscribe', {
        'instrument_id': data.instrument_id,
        'quantity': data.quantity,
        'row_id': rowId } );

};

$('.ui.modal')
    .modal({
        inverted: true,
        onApprove: function(val){
            console.log(val)
        }
    })
    .modal('attach events', '#addInst', 'show')
;


$('.ui.modal .content')
    .api({
        action: 'instrument grid',
        method: 'GET',
        on: 'now',
        onSuccess: function(res) {
            $('.ui.modal .content').append(res.data);
            $('.ui.modal .content .menu .item').tab();
            $('.add')
                .api({
                    action: 'req subscribe',
                    onSuccess: function(res) {
                        add_row(res);
                        $('.ui.modal').modal('hide');
                        saveLayout();
                    },
                    onFailure: function(response) {
                        console.log(response)
                    }
                })
            ;
        }
    })
;

$('.ui.fluid.container')
    .api({
        action: 'tabs',
        method: 'GET',
        on: 'now',
        onSuccess: function(res) {
            $(this).html(res.data);
            setTabs();
            $('#tabs .gridtab.active').api('query');
            $('.newtab')
                .api({
                    action: 'tabs',
                    method: 'POST',
                    onSuccess: function(res) {
                       $(res.menu).insertBefore('#tabs > button');
                       $('.ui.fluid.container').append(res.content);setTabs();
                    }
                })
            ;
        }
    })
;

function setTabs(){
    $('#tabs .gridtab').tab();
    $('#tabs .gridtab').api({
        action: 'tab',
        method: 'GET',
        dataType: 'json',
        beforeSend: function(settings) {
            if(!$('.tab.segment.active').is(':empty')) {
                $(this).editable({});
                return false;
            }
            return settings
        },
        onSuccess: function (res) {
            var id = $(this).attr('data-tab');
            //todo despose of table properly
            $('#quotetable_wrapper').remove();
            $('.attached.tab[data-tab="'+ id + '"]').html(res.data);
            set_table();
            $.each(res.rows, function (i, row) {
                $.api({
                    action: 'req subscribe',
                    urlData: {
                        id: row.id
                    },
                    on: 'now',
                    onSuccess: function(res) {
                        add_row(res);
                    },
                    onFailure: function(response) {
                        console.log(response)
                    }
                })
            });
        },
    });
};

function makeId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    //todo make sure ids don't overlap
    // var row_id = makeid();
    // while ($.inArray(row_id, row_ids) >= 0)
    //     row_id = makeid();
    // row_ids.push(row_id);
    return text;
};

/////////////////////////////////////////////////////////////////////

$( window ).on('load', function(){
    // socket ===========================================================
    socket = io.connect(null, {
        'sync disconnect on unload' : true,
    });

    socket.on('connect', function () {
        socket.emit('home/hello', function (resp) {
            //console.log(resp);
        });
    });
    socket.on('health', function (data) {
        $('#conn-c') .transition('pulse');
    });
    socket.on('ping', function (data) {
        $('#conn-b') .transition('pulse');
    });
    socket.on('quote', function(data){
        var matches = ko.utils.arrayFilter(quote_rows(), function(item) {
            return data.rowId === item.id;
        });
        $.each( matches, function(i, row){
            row.update_quote(data);
        })
    });
});
